# LESIA-Mag - Magnetic Field Lines [IDL version]

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7234784.svg)](https://doi.org/10.5281/zenodo.7234784)

## Available data

Data should be downloaded from http://maser.obspm.fr/support/expres/mfl/ 

The following precomputed datasets are available:

| Planet  | Model Name | Current Sheet | Links | Bundles | Reference | Notes |
|---------|------------|---------------|-------|---------|-----------|-------|
| Mercury | A12        |               | [A12_lsh](http://maser.obspm.fr/support/expres/mfl/A12_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/A12_lsh.tar.gz) | [Anderson, et al. 2012](#a12) | |
| Jupiter | ISaAC      | CAN81         | [ISaAC_lsh](http://maser.obspm.fr/support/expres/mfl/ISaAC_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/ISaAC_lsh.tar.gz) (6.9 GB) | [Hess et al. 2017](#isaac)<br/>[Connerney at al. 1981](#can81) |  |
| Jupiter | JRM09      | CAN81         | [JRM09_lsh](http://maser.obspm.fr/support/expres/mfl/JRM09_lsh)<br/>[JRM09_msh](http://maser.obspm.fr/support/expres/mfl/JRM09_msh/) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/JRM09_lsh.tar.gz) (4.0 GB)<br/>[tar.gz](http://maser.obspm.fr/support/expres/mfl/JRM09_msh.tar.gz) (4.2 GB) | [Connerney et al. 2018](#jrm09)<br/>[Connerney at al. 1981](#can81) | |
| Jupiter | JRM09      | CON20         | [JRM09_CS2020_msh](http://maser.obspm.fr/support/expres/mfl/JRM09_CS2020_msh/) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/JRM09_CS2020_msh.tar.gz) (3.1 GB) | [Connerney et al. 2018](#jrm09)<br/>[Connerney at al. 2020](#con20) | |
| Jupiter | JRM33      | CON20         | JRM33_msh (work in progress) | JRM33_msh.tar.gz (work in progress) | [Connerney et al. 2021](#jrm33)<br/>[Connerney at al. 2020](#con20) | |
| Jupiter | O6         | CAN81         | [O6_lsh](http://maser.obspm.fr/support/expres/mfl/O6_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/O6_lsh.tar.gz) (1.8 GB) | [Connerney et al. 1992](#o6)<br/>[Connerney at al. 1981](#can81) | |
| Jupiter | VIP4       | CAN81         | [VIP4_lsh](http://maser.obspm.fr/support/expres/mfl/VIP4_lsh)<br/>[VIP4_lat](http://maser.obspm.fr/support/expres/mfl/VIP4_lat) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/VIP4_lsh.tar.gz) (2.0 GB)<br/>[tar.gz](http://maser.obspm.fr/support/expres/mfl/VIP4_lat.tar.gz) (0.8 GB) | [Connerney et al. 1998](#vip4)<br/>[Connerney at al. 1981](#can81) | |
| Jupiter | VIPAL      | CAN81         | [VIPAL_lsh](http://maser.obspm.fr/support/expres/mfl/VIPAL_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/VIPAL_lsh.tar.gz) (1.4 GB) | [Hess et al. 2011](#vipal)<br/>[Connerney at al. 1981](#can81) | |
| Jupiter | VIT4       | CAN81         | [VIT4_lsh](http://maser.obspm.fr/support/expres/mfl/VIT4_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/VIT4_lsh.tar.gz) (1.9 GB) | [Connerney 2007](#vit4)<br/>[Connerney at al. 1981](#can81) | |
| Saturn  | SPV        |               | [SPV_lsh](http://maser.obspm.fr/support/expres/mfl/SPV_lsh)<br/>[SPV_lat](http://maser.obspm.fr/support/expres/mfl/SPV_lat) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/SPV_lsh.tar.gz) (2.0 MB)<br/>[tar.gz](http://maser.obspm.fr/support/expres/mfl/SPV_lat.tar.gz) (3.3 MB) | [Davis and Smith. 1990](#spv) |  |
| Saturn  | Z3         |               | [Z3_lsh](http://maser.obspm.fr/support/expres/mfl/Z3_lsh)<br/>[Z3_lat](http://maser.obspm.fr/support/expres/mfl/Z3_lat) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/Z3_lsh.tar.gz) (0.2 MB)<br/>[tar.gz](http://maser.obspm.fr/support/expres/mfl/Z3_lat.tar.gz) (3.4 MB) | [Connerney et al. 1984](#z3)| |
| Uranus  | AH5        |               | [AH5_lsh](http://maser.obspm.fr/support/expres/mfl/AH5_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/AH5_lsh.tar.gz) | [Herbert, 2009](#ah5) | (1) |
| Uranus  | Q3         |               | [Q3_lsh](http://maser.obspm.fr/support/expres/mfl/Q3_lsh) | [tar.gz](http://maser.obspm.fr/support/expres/mfl/G3_lsh.tar.gz) (1.8 GB) | [Connerney et al. 1987](#q3) | (1) | 
| Exoplanet  | TDS         |               | [TDS_lsh](http://maser.obspm.fr/support/expres/mfl/TDS_lsh/) | [tar.gz](https://maser.obspm.fr/support/expres/mfl/TDS_lsh.tar.gz) (66 MB) | [Hess & Zarka, 2011](#tds) | (2) | 
| Exoplanet  | TDS_axi         |               | [TDS_axi_lsh](http://maser.obspm.fr/support/expres/mfl/TDS_axi_lsh/) | [tar.gz](https://maser.obspm.fr/support/expres/mfl/TDS_axi_lsh.tar.gz) (188 KB) | [Hess & Zarka, 2011](#tds) | (2) | 
| Exoplanet  | proxima_centauri         |               | [proxima_centauri_lsh](http://maser.obspm.fr/support/expres/mfl/proxima_centauri_lsh/) | [tar.gz](https://maser.obspm.fr/support/expres/mfl/proxima_centauri_axi_lsh.tar.gz) |  [Reiner & Basri, 2008](#proxima_centauri) | (3) |

Notes:
- (1): Content limited to Ariel-related magnetic field lines. Contact the [MASER team](mailto:contact.maser@obspm.fr) for details.
- (2): Content limited to magnetic field lines of L shell 20. Contact the [MASER team](mailto:contact.maser@obspm.fr) for details.
- (3): Content limited to magnetic field lines of L shell 68. Contact the [MASER team](mailto:contact.maser@obspm.fr) for details.

## How to calculate the Magnetic Field Lines for a given body and a given magnetic field model (+ optionnal current sheet model)

To be able to calculate the magnetic field lines, you need to use the **CALC_MFL.pro** routine.

The mandatory parameters are the following:
- mfl_model: name of the magnetic field model (available models: models=['VIT4','VIP4','O6','VIPAL','ISaAC','JRM09','SPV','Z3','MMMAnderson'])
- lat, msh, lsh or moon: one of these parameters need to be set to 1 (or a name of a Jupiter's moon).
  - lat: Calculation of the magnetic field lines using Colatitude (origin=North pole).
  - lsh: Calculation of the magnetic field lines using apex (=distance in planetary radius) along the planetocentric equator (should only be used for dipolar magnetic field model)
  - msh: Calculation of the magnetic field lines using apex (=distance in planetary radius) along the magnetic equator.
  - moon: name of the desire moon (only avalaible for the Jupiter's moons, i.e. 'Io', 'Europa' or 'Ganymede')
- l1, l2: boundaries for the calculation: if lat=1: colatitude (in degree), if msh=1 or lsh=1 (in planetary radius) - not required if Moon is set to a jovian moon
- nocrt: if no current sheet model is desired (default = 1). The current sheet model is set in the coeff_XXX.pro routine (XXX = name of the magnetic field model)

Example:

- How to calculate the magnetic apex (M-shell) of Jupiter's magnetic field lines, using the model JRM09 (+ CAN81 current sheet model), from M-shell = 2 to M-shell = 50:

`CALC_MFL, l1=2, l2=50, mfl_model='JRM09', /msh`

- How to calculate the apex of Io's (Jovian moon) magnetic field lines, using the model JRM09 (+ CAN81 current sheet model):

`CALC_MFL, moon='Io', mfl_model='JRM09',`


The outputs are described in the next Section (**Directory Setup**)
## Directory setup

```
(1) FOLDERS XXX_lat or XXX_lsh or XXX_msh or XXX_name-of-the-moon (XXX=model's name)
|
|	contain field lines mapping at a given latitude on the planet (lat from 1 to 90 deg by 1deg step)
|	or a given distance in the equatorial (planeto-centric or magnetic) plane (lsh or msh from 2 to 50 by 1 planet radius step)
|
|_(2) Either FOLDERS (0,1,2,...N) or files (0,1,2,...N, -0,-1,...)
   |
   |	numbers correspond to lat or lsh, folder for non axisymmetric, files for axisymmetric (sign=north/south)
   |
   |_ files (0,1,2,...359, -0,-1,...,-359)
	numbers correspond to longitudes (sign to north/south)

	field lines are stored from 2.9*(1E-3 *dip moment in Gauss) MHz to the max frequency by steps 
	of 2.9*(1E-3 *dip moment in Gauss) MHz
```

## Data format
```
Format: Binary file, big endian
Variables:
  LONG   [1]  n = number of data points in the file
  FLOAT  [n]  x = x position of the points
  FLOAT  [n]  y = y position of the points
  FLOAT  [n]  z = z position of the points
  FLOAT  [3,n] b = b unit vector
  FLOAT  [n]  f = frequencies
  FLOAT  [n]  Gwc = delta ln(wc) over c/wc
  FLOAT  [3,n] bz = b basis zenith vector
  FLOAT  [3,n] gb = grad b direction (in b basis)
```

## References

- <a name='a12'></a>Anderson, B. J.,  C. L. Johnson, H. Korth, R. M. Winslow, J. E. Borovsky, M. E. Purucker, J. A. Slavin, S. C. Solomon, M. T. Zuber, and R. L. McNutt (**2012**). _Low‐degree structure in Mercury's planetary magnetic field._ J. Geophys. Res., 117, E00L12, [doi:10.1029/2012JE004159](https://doi.org/10.1029/2012JE004159).
- <a name='vit4'></a>Connerney, J E P. (**2007**). _Planetary Magnetism._ In _Treatise on Geophysics: Planets and Moons_, edited by Tilman Spohn, 10, 243–80. Elsevier.
- <a name='o6'></a>Connerney, J. E. P. (**1992**). _Doing more with Jupiter's magnetic field_, in Planetary Radio Emissions III, edited by S. J. Bauer and H. O. Rucker, pp. 13-33, Austria Acad. of Sci. Press, Vienna. [URL](https://austriaca.at/?arp=0x0015cd0e).
- <a name='can81'></a>Connerney, J. E. P., M. H. Acuna, and N. F. Ness (**1981**). _Modeling the Jovian current sheet and inner magnetosphere._ J. Geophys. Res., 86(A10), 8370-8384. [doi:10.1029/JA086iA10p08370](http://doi.org/10.1029/JA086iA10p08370).
- <a name='z3'></a>Connerney, J. E. P., M. H. Acuna, and N. F. Ness (**1984**). _The Z3 Model of Saturn's Magnetic Field and the Pioneer 11 Vector Helium Magnetometer Observations._ J. Geophys. Res., 89(A9), 7541–7544. [doi:10.1029/JA089iA09p07541](https://doi.org/10.1029/JA089iA09p07541).
- <a name='q3'></a>Connerney, J. E. P., M. H. Acuna, and N. F. Ness (**1987**). _The magnetic field of Uranus_. J. Geophys. Res., 92(A13), 15329-15336. [doi:10.1029/JA092iA13p15329](https://doi.org/10.1029/JA092iA13p15329).
- <a name='vip4'></a>Connerney, J. E. P., M. H. Acuna, N. F. Ness, and T. Satoh (**1998**). _New Models of Jupiter's Magnetic Field Constrained by the Io Flux Tube Footprint._ J. Geophys. Res., 103(A6), 11929–11939. [doi:10.1029/97JA03726](https://dx.doi.org/10.1029/97JA03726).
- <a name='jrm09'></a>Connerney, J. E. P., S. Kotsiaros, R. J. Oliversen, J. R. Espley, J. L. Joergensen, P. S. Joergensen, J. M. G. Merayo, et al. (**2018**). _A New Model of Jupiter's Magnetic Field From Juno's First Nine Orbits._ Geophys. Res. Lett., 45(6), 2590–2596. [doi:10.1007/s11214-009-9621-7](https:/dx.doi.org/10.1007/s11214-009-9621-7).
- <a name='jrm33'></a>Connerney, J. E. P., Timmins, S., Oliversen, R. J., Espley, J. R., Joergensen, J. L., Kotsiaros, S., et al. (**2021**). _A New Model of Jupiter's Magnetic Field at the Completion of Juno's Prime Mission._ Journal of Geophysical Research: Planets, 126, e2021JE007055. [doi:10.1029/2021JE007055](https://doi.org/10.1029/2021JE007055).
- <a name='con20'></a> Connerney, J. E. P., S. Timmins, M. Herceg and J. L. Joergensen (**2020**). _A Jovian magnetodisc model for the Juno era._ J. of Geophys. Res., 125, [doi:10.1029/2020JA028138](https://doi.org/10.1029/2020JA028138).
- <a name='spv'></a>Davis, L. Jr, and E. Smith (**1990**). _A Model of Saturn's Magnetic Field Based on All Available Data._ J. Geophys. Res., 95, 15257–15261. [doi:10.1029/JA095iA09p15257](https://dx.doi.org/10.1029/JA095iA09p15257).
- <a name='ah5'></a>Herbert, F. (**2009**). _Aurora and magnetic field of Uranus._ J. Geophys. Res., 114, A11206, [doi:10.1029/2009JA014394](https://doi.org/10.1029/2009JA014394).
- <a name = 'tds'></a>Hess, S. L. G. and P. Zarka (**2011**). _Modeling the radio signature of the orbital parameters, rotation, and magnetic field of exoplanets_, A&A. [doi:10.1051/0004-6361/201116510 ](https://doi.org/10.1051/0004-6361/201116510).
- <a name='isaac'></a>Hess, S. L. G.,  B. Bonfond, F. Bagenal, and L. Lamy (**2017**). _A Model of the Jovian Internal Field Derived from in-situ and Auroral Constraints_, PRE8 Proceedings, Austrian Academy of Science. [doi:10.1553/PRE8s157](https://dx.doi.org/10.1553/PRE8s157).
- <a name='vipal'></a>Hess, S. L. G., B. Bonfond, P. Zarka, and D. Grodent (**2011**). _Model of the Jovian Magnetic Field Topology Constrained by the Io Auroral Emissions._ J. Geophys. Res., 116(A5), 177. [doi:10.1029/2010JA016262](https://dx.doi.org/10.1029/2010JA016262).
- <a name='proxima_centauri'></a>Reiners, A and G. Basri (**2008**). _The moderate magnetic field of the flare star Proxima Centauri_. A&A 489, L45–L48. [doi:10.1051/0004-6361:200810491](http://dx.doi.org/10.1051/0004-6361:200810491).
