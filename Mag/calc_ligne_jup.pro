pro L_SHELL_REG

m=511.0
Rj=71492000
;Rj=60268000
c2=(299792458.0)^2
fsb=2.79924835996



;**************** OVALE SUD **********************
;data=fltarr(2,72) 
;openr,unit,'/Users/sebastien/Desktop/ovale_princip_sudSTIS_5deg.txt',/get_lun
;readf,unit,data    
;close,unit
;free_lun,unit

;data2=fltarr(360)
;rampe=findgen(5)*0.25
;rampe2=1-findgen(5)*0.25
;for i=0,70 do begin
;data2[5*i:5*i+4]=rampe2*data[0,i]+rampe*data[0,i+1]
;endfor
;data2[5*71:5*71+4]=rampe2*data[0,71]+rampe*data[0,0]
;data2=90.-data2
;**************************************************

;***************OVALE NORD ***********************
;data=fltarr(2,72) 
;openr,unit,'/Users/sebastien/Desktop/ovale_princip_nordSTIS_2deg.txt',/get_lun
;readf,unit,data    
;close,unit
;free_lun,unit
;for i=0,178 do data2[2*i+1]=0.5*(data[0,i]+data[0,i+1])
;data2[359]=0.5*(data[0,179]+data[0,0])
;*************************************************



for p=0,359 do begin


;*********** pour satellites *******

;r_sat >0=nord <0=sud

r_sat=-5.95 ;io							/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
;r_sat=9.5 ;europe
r_sat= 15. ; ganymede
;************************************

;************ pour ovale ***********
;r_sat=1 ;ovale nord
;r_sat=-1 ;ovale_sud
;***********************************

; Mod�le de champ magn�tique:
mfl_model='VIT4';						/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\

;****** si satellite *******
L_SHELL_TEST,r_sat,p,90.,rtp,x,y,z,B,bmod,mfl_model=mfl_model
;****************************

;****** si pas satellite ******** 
;POS_EQ,sqrt(0.945106*sin(data2(p))^2+cos(data2(p))^2),data2(p),p,bceq,bmeq,pos_ceq,pos_meq,mfl_model=mfl_model
; N.B: distance � la plan�te (1er argument de pos_eq) = faux 
;L_SHELL_TEST,rsat*pos_ceq(0),pos_meq(2),pos_meq(1),rtp,x,y,z,B,bmod,mfl_model=mfl_model
;*********************************


;print,max(bmod)
;print,'max',max(rtp[*,0]),max(rtp[*,1]),max(rtp[*,2])
;print,max(x),max(y),max(z)


n=n_elements(bmod)
print,p,'/359 ',n
bmod=bmod*fsb
b=b*fsb
b=transpose(b)


;parce que pour f<1MHz on a pas besoin d'une grande precision spatiale
w=where(bmod lt 1.)
w2=where(bmod ge 1.)
nw=n_elements(w)
msk=[w(indgen(nw/10)*10),w2]
x=x(msk)
y=y(msk)
z=z(msk)
rtp=rtp[msk,*]
bmod=bmod(msk)
b=b[*,msk]
n=n_elements(bmod)

;conversion rtp->xyz et normalisation
rtp(*,1:2)=rtp(*,1:2)*!pi/180.
convert,b,b2,rtp,n
b=b2
b(0,*)/=bmod
b(1,*)/=bmod
b(2,*)/=bmod
;***********************************

;**PS: ne pas oublier de renommer le dossier en fonction de la source

if (r_sat gt 1) then begin
	nom_fichier='/Users/clouis/Documents/SERPE/SERPE_version/IFT_'+mfl_model+'/L=5.95/'+STRSPLIT(STRING(p),/EXTRACT)+'.lsr' ; /!\/!\/!\/!\/!\/!\
	openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
	writeu,unit,n,x,y,z,b,bmod
;	writeu,unit,x
;	writeu,unit,y
;	writeu,unit,z
;	writeu,unit,b
;	writeu,unit,bmod
	close, unit & free_lun, unit
endif else begin
	nom_fichier='/Users/clouis/Documents/SERPE/SERPE_version/IFT_'+mfl_model+'/L=5.95/-'+STRSPLIT(STRING(p+1),/EXTRACT)+'.lsr' ; Attention +1
	openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
	writeu,unit,n
	writeu,unit,x
	writeu,unit,y
	writeu,unit,z
	writeu,unit,b
	writeu,unit,bmod
	close, unit & free_lun, unit
endelse
endfor
;********************************************************************



return
end



pro convert,brtp,bxyz,xyz,n
bxyz=brtp*0.
;xyz_to_rtp,xyz(0,*),xyz(1,*),xyz(2,*),r,theta,phi
r=xyz(*,0)
theta=xyz(*,1)
phi=xyz(*,2)
bxyz(2,*)=cos(theta)*brtp(0,*)-sin(theta)*brtp(1,*)
bxyz(0,*)=sin(theta)*cos(phi)*brtp(0,*)+cos(theta)*cos(phi)*brtp(1,*)-sin(phi)*brtp(2,*)
bxyz(1,*)=sin(theta)*sin(phi)*brtp(0,*)+cos(theta)*sin(phi)*brtp(1,*)+cos(phi)*brtp(2,*)

return
end
