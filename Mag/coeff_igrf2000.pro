pro COEFF_IGRF2000,g,h,n,dip,crt,tdip

;coefficients of IGRF2000 model
;g et h:coeff of Legendre polynomes
;n:development order
;dip:position of the dipole wrt the rotation axis
;crt: current sheet

G=fltarr(6,6) & H=fltarr(6,6)
n=3
G(1,0)=-0.29615
G(1,1)=-0.01728 &  H(1,1)=0.05186
G(2,0)=-0.02267
G(2,1)=0.03072  &  H(2,1)=-0.02478
G(2,2)=0.01672  &  H(2,2)=-0.00458
G(3,0)=0.01341
G(3,1)=-0.02290 &  H(3,1)=-0.00227
G(3,2)=0.01253  &  H(3,2)=0.00296
G(3,3)=0.00715  &  H(3,3)=-0.00492

; tilt = 11¡ et longitude ouest prise = 100¡
dip=[11,100]
crt=[0,0,0,0]

; Magnetic/rotation coordinate change matrix:
; tdip = matrix of passage from the magnetic axis to the rotation axis  (a=-100/b=11) = 
; [cosa.cosb -sina cosa.sinb]
; [sina.cosb  cosa sina.sinb]
; [    -sinb     0      cosb]

  tdip=[[-0.170458,0.984808,-0.0331336],$
	[-0.966714,-0.173648,-0.187910],$
	[-0.190809,0.,0.981627]]


return
end
