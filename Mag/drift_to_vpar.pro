;----------------------------------------------------------------------------  pro DRIFT_TO_VPAR,f,df_dt,L,longitude,vpar,mfl_model=mfl_model,nocrt=nocrt
;----------------------------------------------------------------------------
; (INPUT)
; f = scalar or 1D array of frequencies (MHz)
; df_dt = scalar or 1D array of drift rates (MHz/s)
; L = L-shell of source (5.9 for Io-Jupiter emission) : L>0 --> North hemisphere, L<0 --> South hemisphere
; longitude (deg) = system III longitude of assumed source (cf. MOP2002.ppt)
; mfl_model = 'D', 'O6', 'VIP4', 'VIPAL' = magnetic field model
; /nocrt allows NOT to take into account the current disc
; (OUTPUT)
; vpar = scalar or 1D array (same size as f & df_dt) of V// (km/sec)

Rj=71400.	; Jupiter radius
if L ge 0 then theta=45 else theta=135
L_SHELL_TEST,L,longitude,theta,rtp,x,y,z,bv,b,mfl_model=mfl_model,nocrt=nocrt
n=n_elements(b)
fc=2.8*(b(0:n-2)+b(1:n-1))/2.
df=2.8*(b(1:n-1)-b(0:n-2))
ds=sqrt((x(1:n-1)-x(0:n-2))^2+(y(1:n-1)-y(0:n-2))^2+(z(1:n-1)-z(0:n-2))^2)*Rj

m=n_elements(f)
vpar=fltarr(m)
for i=0,m-1 do begin
  w=where(abs(fc-f(i)) eq min(abs(fc-f(i))))
  w=w(0)
  vpar(i)=-df_dt(i)/(df(w)/ds(w))
endfor
return
end