; data_set = read_csv("/Users/serpe/Volumes/kronos/juno/CL_work/jonas_rabia_internship/juno_position_reorganize.csv", header = sed_header, n_table_header = 1, table_header= SedTableHeader)
; date = data_set.field2
; x_init = data_set.field3 
; y_init = data_set.field4 
; z_init = data_set.field5
; magnetic_field_model = 'JRM09'

FUNCTION calc_m_shell_associated_with_any_position,x_init,y_init,z_init, magnetic_field_model = magnetic_field_model, M_shell = M_shell, L_shell = L_shell
	print,magnetic_field_model +' model will be used'
	TIC

    shell_value = []
	r = sqrt(x_init^2+y_init^2+z_init^2)    
	theta = atan(sqrt(x_init^2+y_init^2),z_init) *180./!pi  
	phi = (360-atan(y_init,x_init)*180./!pi) mod 360. 

	COEFF,g,h,n,dip,crt,tdip,mfl_model=magnetic_field_model 
	dip_param={G:G,$
              H:H,$
              N:N,$
              dip:dip,$
              crt:crt,$
              tdip:tdip}

    thetastop=(dip[0])*cos((dip[1]-phi)*!dtor)+90.
    

    for i=0,n_elements(r)-1 do begin
    	print,strtrim(i+1,2)+" / "+strtrim(n_elements(r))
    	if (r[i] ge 2) and (r[i] le 70) then begin
            if keyword_set(M_shell) then begin
                L_SHELL_TEST_N,r[i],phi[i],theta[i],rtp,x,y,z,bv,b,gb,bmin,mfl_model=magnetic_field_model,/msh,lat_stop=thetastop[i],dip_param=dip_param,h0=-0.01
    		    shell_value = [shell_value,rtp[-2,0]]
            endif
            if keyword_set(L_shell) then begin
                L_SHELL_TEST_N,r[i],phi[i],theta[i],rtp,x,y,z,bv,b,gb,bmin,mfl_model=magnetic_field_model,/lsh,lat_stop=thetastop[i],dip_param=dip_param,h0=-0.01
                shell_value = [shell_value,rtp[-2,0]]
            endif
    	endif else begin
            shell_value = !values.f_nan
        endelse
    endfor
    TOC
    return, shell_value


END