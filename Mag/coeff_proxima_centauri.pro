pro COEFF_proxima_centauri,g,h,n,dip,crt,tdip

; # coefficients for a 600 Gauss dipole
; # based on M. Pérez-Torres et al. (2021) https://doi.org/10.1051/0004-6361/202039052
; # g et h: coefficients of Legendre polynomials
; # n: development order
; # dip: position of the dipole/axis of rotation
; # crt: current sheet

G=fltarr(6,6) & H=fltarr(6,6)
n=3
G(1,0)=600.0
G(1,1)=-0.0  &  H(1,1)=0.0
G(2,0)=-0.0
G(2,1)=-0.0  &  H(2,1)=-0.0
G(2,2)=0.0  &  H(2,2)=0.0
G(3,0)=0.0
G(3,1)=-0.0  &  H(3,1)=-0.0
G(3,2)=0.0  &  H(3,2)=0.0
G(3,3)=-0.0  &  H(3,3)=-0.0

dip=[15.,0.]
crt=[0.,0.,0.,0.]


 
 tdip=[[1.,0.,0.],$
	[0.,1.,0.],$
	[0.,0.,1.]]

return
end
