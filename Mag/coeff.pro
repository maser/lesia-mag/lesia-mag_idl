pro COEFF, g,h,n,dip,crt,tdip,mfl_model=mfl_model

if ~ keyword_set(mfl_model) then stop,'You must choose a magnetic field model. Aborting...'
case strupcase(mfl_model) of
  ; Mercure
  'A12' : coeff_A12, g,h,n,dip,crt,tdip ; #Mercury Magnetic field Model from Anderson et al. [2011, 2012]
  ; Terre
  'IGRF1995'   : coeff_IGRF1995, g,h,n,dip,crt,tdip
  'IGRF2000'   : coeff_IGRF2000, g,h,n,dip,crt,tdip
  ; Exoplanet
  'TDS'   : coeff_tds, g,h,n,dip,crt,tdip
  'TDS_AXI'   : coeff_tds_axi, g,h,n,dip,crt,tdip
  'PROXIMA_CENTAURI': COEFF_proxima_centauri,g,h,n,dip,crt,tdip
  ; Jupiter:
  'D'            : COEFF_D, g,h,n,dip,crt,tdip
  'O6'           : COEFF_O6, g,h,n,dip,crt,tdip
  'VIP4'         : COEFF_VIP4, g,h,n,dip,crt,tdip
  'VIT4'         : COEFF_VIT4, g,h,n,dip,crt,tdip
  'VIPAL'        : COEFF_VIPAL, g,h,n,dip,crt,tdip
  'ISAAC'        : COEFF_ISAAC, g,h,n,dip,crt,tdip
  'JRM09'        : COEFF_JRM09,g,h,n,dip,crt,tdip
  'JRM09_CS2020' : COEFF_JRM09_CSCON2020,g,h,n,dip,crt,tdip
  'JRM33'        : COEFF_JRM33,g,h,n,dip,crt,tdip
  ; Saturne:
  'Z3'    : COEFF_Z3, g,h,n,dip,crt,tdip
  'SPV'   : COEFF_SPV, g,h,n,dip,crt,tdip
  'SPVR'  : COEFF_SPVR, g,h,n,dip,crt,tdip
  ; Uranus:
  'Q3'    : COEFF_Q3, g,h,n,dip,crt,tdip
  'AH5'   : COEFF_AH5, g,h,n,dip,crt,tdip
  ; test cases 
  'TEST': coeff_test, g,h,n,dip,crt,tdip
  'TEST_DIP_Z_1T': coeff_test_dip_z_1t, g,h,n,dip,crt,tdip
endcase

return
end
