r=1.
theta=findgen(89)+1
phi=findgen(360)
bb=fltarr(360,89)
for i=0,88 do begin
print,i
for j=0,359 do begin
th=theta(i)
ph=phi(j)
MAGNETIC_FIELD, R,TH,PH, B_SPH,B,br,mfl_model='TEST',/nocrt
bb(j,i)=b
endfor
endfor
contour,bb,phi,theta,/xsty,/ysty,levels=findgen(50.)/10.
stop

l=6 & colatitude=90.
rtpp=fltarr(120,3) & bp=fltarr(120) & n=bp
for longitude=0,357,3 do begin
print,longitude
L_SHELL_TEST,l,longitude,colatitude,rtp,x,y,z,bv,b,mfl_model='TEST'
j=longitude/3
n(j)=n_elements(rtp(*,0))-1
bp(j)=b(n(j))
rtpp(j,*)=rtp(n(j),*)
endfor
end