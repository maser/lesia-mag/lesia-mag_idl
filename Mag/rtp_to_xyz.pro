; --------------------------------------
  pro RTP_TO_XYZ, r,theta,phi, x,y,z; --------------------------------------
; x,y,z & r in Rj; theta, phi in rad
  r=r*1. & theta=theta*1. & phi=phi*1.  x=r*sin(theta)*cos(phi)  y=r*sin(theta)*sin(phi)  z=r*cos(theta)returnend