pro COEFF_IGRF1995,g,h,n,dip,crt,tdip
;coefficients du Dipole de 7 Gauss (Z96)
;g et h:coeff. des polynomes de Legendre
;n:ordre du developpement
;dip:position du dipole/axe de rotation
;crt:anneau de courant
G=fltarr(6,6) & H=fltarr(6,6)
n=5
G(1,0)=-0.29692
G(1,1)=-0.01784 &  H(1,1)=0.05306
G(2,0)=-0.02200
G(2,1)=0.03070  &  H(2,1)=-0.02366
G(2,2)=0.01681  &  H(2,2)=-0.00413
G(3,0)=0.01335
G(3,1)=-0.02267 &  H(3,1)=-0.00262
G(3,2)=0.01249  &  H(3,2)=0.00302
G(3,3)=0.00759  &  H(3,3)=-0.00427
G(4,0)=0.00940
G(4,1)=0.00780  &  H(4,1)=0.00262
G(4,2)=0.00290  &  H(4,2)=-0.00236
G(4,3)=-0.00418 &  H(4,3)=-0.00097
G(4,4)=0.00122  &  H(4,4)=-0.00306
G(5,0)=-0.00214
G(5,1)=0.00352  &  H(5,1)=0.00046
G(5,2)=0.00235  &  H(5,2)=0.00165
G(5,3)=-0.00118 &  H(5,3)=-0.00143
G(5,4)=-0.00166 &  H(5,4)=-0.00055
G(5,5)=-0.00017 &  H(5,5)=0.00107

; tilt = 11� et longitude ouest prise = 100�
dip=[11,-100]
crt=[0,0,0,0]
; tdip = matrice de passage de l'axe magn�tique � l'axe de rotation (a=-100/b=11) =
; [cosa.cosb -sina cosa.sinb]
; [sina.cosb  cosa sina.sinb]
; [    -sinb     0      cosb]
; Application num�rique � rev�rifier avant utilisation
  tdip=[[-0.170458,0.984808,-0.0331336],$
    [-0.966714,-0.173648,-0.187910],$
    [-0.190809,0.,0.981627]]
return
end