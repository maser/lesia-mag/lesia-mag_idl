PRO mfl_map,calc=calc, model=model,Io=Io,Europa=Europa,Ganymede=Ganymede,Callisto=Callisto


if keyword_Set(calc) then begin
	theta=findgen(181,start=-90,inc=+1)
	longitude=findgen(360)
	b=fltarr(360,181)
	e=0.0648744
	r_th_elips=1./sqrt((cos((theta)*!dtor)^2.+(sin((theta)*!dtor)/(1-e))^2.))*1.000
	for i=0,n_elements(theta)-1 do begin
	for j=0,n_elements(longitude)-1 do begin
	print,theta[i],longitude[j],r_th_elips[i]
	magnetic_field_N,r_th_elips[i],90.-theta[i],(360.-longitude[j]) mod 360,b_sph,b_tmp,br,g,h,mfl_model=model
	b[j,i]=b_tmp
	endfor
	endfor
	
	theta=transpose(rebin(theta,181,360))
	longitude=rebin(longitude,360,181)
	save,r_th_elips,longitude,theta,b,filename='map_'+strlowcase(model)+'.sav'
endif

if n_elements(model) ge 3 then !p.multi=[0,2,2]
if n_elements(model) eq 2 then !p.multi=[0,1,2]

set_plot,'ps'
if n_elements(model) eq 1 then name=model+'_map' else name='multi_model_map'
device,filename=name+'.ps',/landscape,bits=8,/col,/Helvetica

for i=0,n_elements(model)-1 do begin
	if keyword_Set(Io) then begin
		read_mfl,r_ftpN,lon_ftpN,lat_ftpN,x_ftpN,y_ftpN,r_ftpS,lon_ftpS,lat_ftpS,x_ftpS,y_ftpS,lon,fc_ftpS=fc_ftpS,fc_ftpN=fc_ftpN,path='/Groups/SERPE/data/mfl/'+model[i]+'_lsh/Io/',mfl_model=model[i]
		lon_ftpN=(lon_ftpN+360) mod 360.
		lon_ftpS=(lon_ftpS+360) mod 360.
		latIoN=lat_ftpN(sort(lon_ftpN))
		lonIoN=lon_ftpN(sort(lon_ftpN))
	
		latIoS=lat_ftpS(sort(lon_ftpS))
		lonIoS=lon_ftpS(sort(lon_ftpS))
	endif
	if keyword_Set(Europa) then begin
		read_mfl,r_ftpN,lon_ftpN,lat_ftpN,x_ftpN,y_ftpN,r_ftpS,lon_ftpS,lat_ftpS,x_ftpS,y_ftpS,lon,fc_ftpS=fc_ftpS,fc_ftpN=fc_ftpN,path='/Groups/SERPE/data/mfl/'+model[i]+'_lsh/Europa/',mfl_model=model[i]
		lon_ftpN=(lon_ftpN+360) mod 360.
		lon_ftpS=(lon_ftpS+360) mod 360.
		latEuN=lat_ftpN(sort(lon_ftpN))
		lonEuN=lon_ftpN(sort(lon_ftpN))
	
		latEuS=lat_ftpS(sort(lon_ftpS))
		lonEuS=lon_ftpS(sort(lon_ftpS))
	endif
	if keyword_Set(Ganymede) then begin
		read_mfl,r_ftpN,lon_ftpN,lat_ftpN,x_ftpN,y_ftpN,r_ftpS,lon_ftpS,lat_ftpS,x_ftpS,y_ftpS,lon,fc_ftpS=fc_ftpS,fc_ftpN=fc_ftpN,path='/Groups/SERPE/data/mfl/'+model[i]+'_lsh/15/',mfl_model=model[i]
		lon_ftpN=(lon_ftpN+360) mod 360.
		lon_ftpS=(lon_ftpS+360) mod 360.
		if model[i] eq 'VIPAL' or model[i] eq 'VIP4' then begin
			latGaN=lat_ftpN
			lonGaN=lon_ftpN
		endif else begin
			latGaN=lat_ftpN(sort(lon_ftpN))
			lonGaN=lon_ftpN(sort(lon_ftpN))
		endelse
		latGaS=lat_ftpS(sort(lon_ftpS))
		lonGaS=lon_ftpS(sort(lon_ftpS))
	endif
	
	if keyword_Set(Callisto) then begin
		read_mfl,r_ftpN,lon_ftpN,lat_ftpN,x_ftpN,y_ftpN,r_ftpS,lon_ftpS,lat_ftpS,x_ftpS,y_ftpS,lon,fc_ftpS=fc_ftpS,fc_ftpN=fc_ftpN,path='/Groups/SERPE/data/mfl/'+model[i]+'_lsh/Callisto/',mfl_model=model[i]
		lon_ftpN=(lon_ftpN+360) mod 360.
		lon_ftpS=(lon_ftpS+360) mod 360.
		latCaN=lat_ftpN
		lonCaN=lon_ftpN
	
		latCaS=lat_ftpS(sort(lon_ftpS))
		lonCaS=lon_ftpS(sort(lon_ftpS))
	endif


 	if ~keyword_set(calc) then restore,'map_'+strlowcase(model[i])+'.sav'
	if n_elements(model) ge 3 then begin
		if i eq 0 then contour,b,longitude,theta,/nodata,ytit='Latitude',charthick=3.5,/xsty,/ysty,thick=3.5,xthick=3.5,ythick=3.5,tit=model[i]+' map',XTICKFORMAT="(A1)"
		if i eq 1 then contour,b,longitude,theta,/nodata,charthick=3.5,/xsty,/ysty,thick=3.5,xthick=3.5,ythick=3.5,tit=model[i]+' map',XTICKFORMAT="(A1)",YTICKFORMAT="(A1)"
		if i eq 2 then contour,b,longitude,theta,/nodata,xtit='Longitude',ytit='Latitude',charthick=3.5,/xsty,/ysty,thick=3.5,xthick=3.5,ythick=3.5,tit=model[i]+' map'
		if i eq 3 then contour,b,longitude,theta,/nodata,xtit='Longitude',charthick=3.5,/xsty,/ysty,thick=3.5,xthick=3.5,ythick=3.5,tit=model[i]+' map',YTICKFORMAT="(A1)"
	endif
	loadct,17
	stop
	contour,b,longitude,theta,/xsty,/ysty,level=[0,2,4,6,8,10,12,14,16,18,20],c_annot=['0','2','4','6','8','10','12','14','16','18','20'],C_Col=[255,225,200,175,150,125,100,75,50,25,0],/cell_fill,c_thick=3.5,c_charsize=0.75,c_charthick=3.5,/overplot,thick=3.5,/closed
	loadct,0
	contour,b,longitude,theta,/xsty,/ysty,level=[0,2,4,6,8,10,12,14,16,18,20],c_annot=['0','2','4','6','8','10','12','14','16','18','20'],c_thick=3.5,c_charsize=0.75,c_charthick=3.5,/overplot,thick=3.5;,/closed
	;contour,b,longitude,theta,/nodata,xtit='longitude',ytit='latitude',charthick=3.5,/xsty,/ysty,thick=3.5,xthick=3.5,ythick=3.5,/noerase,tit=model[i]+' map'
	loadct,5
	if keyword_set(Io) then begin
		oplot,lonIoN,latIoN,thick=5.5,color=100
		oplot,lonIoS,latIoS,thick=5.5,color=100
	endif
	if keyword_set(Europa) then begin
		oplot,lonEuN,latEuN,thick=5.5,color=40
		oplot,lonEuS,latEuS,thick=5.5,color=40
	endif
	if keyword_set(Ganymede) then begin
		oplot,lonGaN,latGaN,thick=5.5,color=65
		oplot,lonGaS,latGaS,thick=5.5,color=65
	endif
	if keyword_set(Callisto) then begin
		oplot,lonCaN,latCaN,thick=5.5,color=193
		oplot,lonCaS,latCaS,thick=5.5,color=193
	endif
endfor

loadct,0
device,/close & set_plot,'x'
spawn,'ps2pdf '+name+'.ps '+name+'.pdf'
spawn,'rm -f '+name+'.ps'

end