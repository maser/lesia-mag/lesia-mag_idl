;+
; Contains the read_mfl procedure
;
; :Author:
;    Laurent Lamy
;
;    2015/03/11: Created
;
;    2015/11/02: Last Edit
;-
;
;+
; Procédure de lecture de lignes de champ (calculées avec les routines associées à SERPE 6.0)
; et extraction des coordonnées des pieds nord et sud du tube de flux
;
;# :Params:
;#    r_ftpN: out, required, type=float
;#       pied nord, distance au barycentre planétaire
;#    lon_ftpN: out, required, type=float
;#       pied nord, longitude
;#    lat_ftpN: out, required, type=float
;#       pied nord, latitude
;#    x_ftpN, y_ftpN: out, required, type=float
;#       pied nord, coordonnées cartesiennes
;#    r_ftpS: out, required, type=float
;#       pied sud, distance au barycentre planétaire
;#    lon_ftpS: out, required, type=float
;#       pied sud, longitude
;#    lat_ftpS: out, required, type=float
;#       pied sud, latitude
;#    x_ftpS, y_ftpS: out, required, type=float
;#       pied sud, coordonnées cartesiennes
;#    lon: out, required, type=float
;#       longitude équatoriale
;#    mfl_model:  in, required, type=sometype
;#       O6/VIP4/VIT4/VIPAL/ISaAC/SPV
;#    apex:  in, required, type=string
;#    longitude: in, optional, type=float
;#       If users want only the values for one longitude
;#       1 to 90
;#    for the following optional parameters, if a value of longitude has been given
;#      the parameters below will return the value at the asked longitude.
;#      Otherwise it will be for longitude = 359°
;#     xyz_mfl: in, optional, type=[]
;#        value of the (x,y,z) points along the magnetic field line
;#     b_mfl: in, optional, type=[]
;#        value of the (x,y,z) component of the magnetic field along the magnetic field line
;#     f_mfl: in, optional, type=[]
;#        value of f_ce along the magnetic field line
;#     gwc: in, optional, type=[]
;#        delta ln(wc) over c/wc along the magnetic field line
;#     bz: in, optional, type=[]
;#        b basis zenith vector along the magnetic field line
;#     gb: in, optional, type=[]
;#        grad b direction (in b basis) along the magnetic field line

;
; :Keywords:
;    fc_ftpN : out, optional, type=float
;       pied sud, fc_max
;    fc_ftpS : out, optional, type=float
;       pied sud, fc_max
;    e: out, optional, type=float
;       applatissement planétaire associée à la planète renseignée
;    path: in, optional, type=string
;       chemin d'accès du dossier de lignes de champ écrites au format i.lsr (i=1,360) 
;       avec les routines associées à SERPE 6.0
;     altitude : in, optional,type=float
;        altitude du pied de la ligne de champ

PRO READ_MFL,r_ftpN,lon_ftpN,lat_ftpN,x_ftpN,y_ftpN,$
             r_ftpS,lon_ftpS,lat_ftpS,x_ftpS,y_ftpS,lon,$
             fc_ftpS=fc_ftpS,fc_ftpN=fc_ftpN,$
             path=path,mfl_model=mfl_model,apex=apex,e=e,planetographic=planetographic, $
             altitude=altitude,$
             xyz_mfl=xyz_mfl,b_mfl=b_mfl,f_mfl=f_mfl,gwc=gwc,bz=bz,gb=gb,longitude=longitude



if ~keyword_set(e) then begin
  if mfl_model eq 'O6' or mfl_model eq 'VIP4' or mfl_model eq 'VIT4' or mfl_model eq 'VIPAL' or mfl_model eq 'ISaAC' or mfl_model eq 'JRM09'  or mfl_model eq 'JRM33' then begin
    e = 0.0648744
    ;alt_aur = 300./71492.
    if ~keyword_set(altitude) then alt_aur=0.  else alt_aur=altitude/71492.
  endif else if mfl_model eq 'SPV' then begin
    e = 0.098
  endif else if mfl_model eq 'proxima_centauri' then begin
    e = 0
  endif else stop,'Unknown Magnetic field model (choose among O6/VIP4/VIT4/VIPAL/ISaAC/SPV)'
endif

if ~keyword_set(path) then path = '/Volumes/Maser-DIO/SERPE_SERVER/data/mfl/'+mfl_model+'_lsh/'+apex+'/'
;#if ~keyword_set(path) then path = '/Users/serpe/Volumes/kronos/serpe/mfl/'+mfl_model+'_lsh/'+strtrim(apex,2)+'/'
if ~keyword_set(altitude) then alt_aur=0.  else alt_aur=altitude/71492.
lon = findgen(360)
lat_ftpN = fltarr(360) & r_ftpN = fltarr(360) & lon_ftpN = fltarr(360) 
lat_ftpS = fltarr(360) & r_ftpS = fltarr(360) & lon_ftpS = fltarr(360) 
x_ftpS   = fltarr(360) & y_ftpS = fltarr(360)
x_ftpN   = fltarr(360) & y_ftpN = fltarr(360)
fc_ftpN  = fltarr(360) & fc_ftpS = fltarr(360)

if ~keyword_set(longitude) then begin
  jmin=0
  jmax=359
endif else begin
  jmin=longitude mod 360
  jmax=longitude mod 360
endelse

for j=jmin,jmax do begin

file=path+strtrim(j,1)+'.lsr'

n=0L
openr,unit, file,/get_lun,/swap_if_little_endian
  readu,unit,n
  b_read      = fltarr(3,n)
  f_read      = fltarr(n)
  x_read      = fltarr(3,n)
  gb_read     = fltarr(n)
  bz_read     = fltarr(3,n)
  sc_gb_read  = fltarr(n)
  xt          = fltarr(n)
  readu,unit, xt
  x_read(0,*) = xt
  readu,unit, xt
  x_read(1,*) = xt
  readu,unit, xt
  x_read(2,*) = xt
  xt          = 0
  readu,unit, b_read
  readu,unit, f_read
  readu,unit,sc_gb_read
  readu,unit,bz_read
  readu,unit,gb_read
close, unit & free_lun, unit

x_mfl=fltarr(n) & y_mfl=x_mfl & z_mfl=x_mfl & fc=x_mfl
bx_mfl=x_mfl & by_mfl=x_mfl & bz_mfl=x_mfl
bzx=x_mfl & bzy=x_mfl & bzz=x_mfl
f_mfl=x_mfl & gwc=x_mfl  & gb=x_mfl
for i=0,n-1l do begin
  x_mfl(i)=x_read(1,n-1l-i)
  y_mfl(i)=x_read(0,n-1l-i)
  z_mfl(i)=x_read(2,n-1l-i)
  fc(i)=f_read(n-1l-i)
  bx_mfl[i]=b_read[0,n-1l-i]
  by_mfl[i]=b_read[1,n-1l-i]
  bz_mfl[i]=b_read[2,n-1l-i]
  gwc[i]=sc_gb_read[n-1l-i]
  bzx[i]=bz_read[0,n-1l-i]
  bzy[i]=bz_read[1,n-1l-i]
  bzz[i]=bz_read[2,n-1l-i]
  gb[i]=gb_read[n-1l-i]
endfor



file=path+'-'+strtrim(j,1)+'.lsr'
n=0L
openr,unit, file,/get_lun,/swap_if_little_endian
  readu,unit,n
  b_read      = fltarr(3,n)
  f_read      = fltarr(n)
  x_read      = fltarr(3,n)
  gb_read     = fltarr(n)
  bz_read      = fltarr(3,n)
  sc_gb_read  = fltarr(n)
  xt          = fltarr(n)
  readu,unit, xt
  x_read(0,*) = xt
  readu,unit, xt
  x_read(1,*) = xt
  readu,unit, xt
  x_read(2,*) = xt
  xt          = 0
  readu,unit, b_read
  readu,unit, f_read
  readu,unit,sc_gb_read
  readu,unit,bz_read
  readu,unit,gb_read
close, unit & free_lun, unit
       
x_mfl=[x_mfl,reform(x_read(1,*))] 
y_mfl=[y_mfl,reform(x_read(0,*))]
z_mfl=[z_mfl,reform(x_read(2,*))]
fc=[fc,f_read]
f_mfl=fc
bx_mfl=[bx_mfl,reform(b_read[0,*])]
by_mfl=[by_mfl,reform(b_read[1,*])]
bz_mfl=[bz_mfl,reform(b_read[2,*])]
gwc=[gwc,sc_gb_read]
bzx=[bzx,reform(bz_read[0,*])]
bzy=[bzy,reform(bz_read[1,*])]
bzz=[bzz,reform(bz_read[2,*])]
gb=[gb,gb_read]
xyz_mfl=fltarr(3,n_elements(x_mfl))
b_mfl=fltarr(3,n_elements(bx_mfl))
bz=fltarr(3,n_elements(bzx))
xyz_mfl[0,*]=x_mfl & xyz_mfl[1,*]=y_mfl & xyz_mfl[2,*]=z_mfl
b_mfl[0,*]=bx_mfl & b_mfl[1,*]=by_mfl & b_mfl[2,*]=bz_mfl
bz[0,*]=bzx & bz[1,*]=bzy & bz[2,*]=bzz

; Portion de la ligne de champ extérieure à la planète > altitude aurores :
if ~keyword_set(planetographic) then llat = atan(z_mfl,sqrt(x_mfl^2+y_mfl^2)) else llat = atan(tan(atan(z_mfl,sqrt(x_mfl^2+y_mfl^2)))/((1.-e)^2.))

r = sqrt(x_mfl^2+y_mfl^2+z_mfl^2)
w = where(r ge (sqrt(1.^2/(cos(llat)^2+(sin(llat)/(1.-e))^2))+alt_aur),n)

x_mfl = x_mfl(w) & y_mfl=y_mfl(w) & z_mfl=z_mfl(w)
fc = fc(w)

lat_ftpN(j) = atan(z_mfl(0)/sqrt(x_mfl(0)^2+y_mfl(0)^2))*!radeg
r_ftpN(j) = sqrt(x_mfl(0)^2+y_mfl(0)^2+z_mfl(0)^2)
lon_ftpN(j) = atan(x_mfl(0),y_mfl(0))*!radeg
fc_ftpN(j) = fc(0)
lat_ftpS(j) = atan(z_mfl(n-1l)/sqrt(x_mfl(n-1l)^2+y_mfl(n-1l)^2))*!radeg
r_ftpS(j) = sqrt(x_mfl(n-1l)^2+y_mfl(n-1l)^2+z_mfl(n-1l)^2)
lon_ftpS(j) = atan(x_mfl(n-1l),y_mfl(n-1l))*!radeg
fc_ftpS(j) = fc(n-1l)
x_ftpS(j) = x_mfl(n-1l)
y_ftpS(j) = y_mfl(n-1l)
x_ftpN(j) = x_mfl(0)
y_ftpN(j) = y_mfl(0)

endfor

end