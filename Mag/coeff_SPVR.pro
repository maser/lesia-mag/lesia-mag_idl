pro COEFF_SPVR, g,h,n,dip,crt,tdip
; coefficients modele SPV-R (tronque a l'ordre 5)
n=5
G=dblarr(6,6) & H=dblarr(6,6)G(1,0)= 0.2116d0G(2,0)= 0.0156d0G(3,0)= 0.0232d0G(3,1)=-0.003d0G(4,1)=-0.004d0G(5,1)=-0.005d0H(4,1)= 0.002d0
returnend