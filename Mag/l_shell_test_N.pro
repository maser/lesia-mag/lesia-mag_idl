; ********************
; *          *
; * L_SHELL_TEST.PRO *
; *          *
; ********************
;***************************************************************
function G_STOP,y,mfl_model
; eps = Applatissement planete
mfl_model=strupcase(mfl_model)
if (mfl_model eq 'D' or mfl_model eq 'O6' or mfl_model eq 'VIP4' or mfl_model eq 'VIT4' or $
    mfl_model eq 'VIPAL' or mfl_model eq 'ISAAC' or mfl_model eq 'JRM09' or mfl_model eq 'JRM09_CS2020' or mfl_model eq 'JRM33') then eps=0.064935
if (mfl_model eq 'TDS') or (mfl_model eq 'TDS_AXI') or (mfl_model eq 'PROXIMA_CENTAURI') then eps=0.
if (mfl_model eq 'IGRF1995') then eps=0.00329257
if (mfl_model eq 'Z3' or mfl_model eq 'SPV' or mfl_model eq 'SPVR') then eps=0.098
if (mfl_model eq 'Q3' or mfl_model eq 'AH5') then eps=0.0229273                     
if (mfl_model eq 'A12' or mfl_model eq 'AH5') then eps=0.0
if (mfl_model eq 'IGRF2000') then eps=0.0033528
rep=y(0)-1.d0/sqrt(1.d0+eps*(2.d0-eps)/(1.d0-eps)^2*(cos(y(1)*!dpi/180.d0))^2)
return,rep
end
;***************************************************************
pro DERIV,y,f,b,bv,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
MAGNETIC_FIELD_N,y(0),y(1),y(2),bv,b,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
; Le mot-clef /nocrt permet de ne pas tenir compte de l'anneau de courant
f=[bv(0),bv(1)/y(0)/!dtor,bv(2)/y(0)/!dtor/sin(y(1)*!dtor)]/b
return
end
;***************************************************************
pro DERIV2,y,gb,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
MAGNETIC_FIELD_N,y(0),y(1),y(2),bv,b000,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
MAGNETIC_FIELD_N,(y(0)+1E-3),y(1),y(2),bv,b100,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
MAGNETIC_FIELD_N,y(0),(y(1)+1E-3),y(2),bv,b010,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
MAGNETIC_FIELD_N,y(0),y(1),(y(2)+1E-3),bv,b001,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
gb=[(b100-b000),(b010-b000)/y[0],(b001-b000)/y[0]/sin(y[1]*!dtor)]*1000.
return
end

;***************************************************************
pro L_SHELL_TEST_N,l,sls,the,rtp,x,y,z,bv,b,gb,bmin,lun=lun,mfl_model=mfl_model,$
    lsh=lsh,msh=msh,nocrt=nocrt,lat_stop=lat_stop,dip_param=dip_param,h0=h0
if n_elements(lun) eq 0 then lun=-1
; INPUT
; l (INPUT)=initial r in Rj
; sls (INPUT)=initial west longitude in degree (will be shift into spherical longitude phi=360°-sls)
; the (INPUT)= initial colatitude (90°-latitude)
; mfl_model (mandatory)= magnetic field model 
; lsh and msh= apex of the magnetic field lines counted from the magnetic or jovicentric equator
; nocrt (optional)= to not take into account the current sheet (default one: CAN model)
; lat_stop (optional)= to break the loop at a particular latitude
; bmin (optional, useful if lat_stop activate)= the loop will be broken if b<bmin
; h0 (optional)= step; if >0 then calcul toward the pole in the same hemisphere, if <0 then calcul toward the pole in the other hemisphere
;OUTPUT
; x,y,z (OUTPUT)=cartesian position of the magnetic field line
; bv (OUTPUT)= magnetic field vector
; b (OUTPUT)= magnetic field module
; gb (OUTPUT)= direction of the gradient of b

; variables initialization
if ~keyword_set(h0) then h0=0.001
eps=1.e-6
y=dblarr(3)
y(0)=abs(l) & y(1)=the & y(2)=360.d0-sls
h00=-abs(l)/l*h0
h=h00
DERIV,y,dydx,b1,bs,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
arret=g_stop(y,mfl_model)
if abs(arret) ge eps and arret ne 0. then sgn=arret/abs(arret) else goto,suite
rtp=[[y(0)],[y(1)],[y(2)]] & drtp=[[dydx(0)],[dydx(1)],[dydx(2)]]
DERIV2,y,gb,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
gb=[[gb[0]],[gb[1]],[gb[2]]]
b=[b1] & bv=[[bs(0)],[bs(1)],[bs(2)]]
if bs[0] ge 0 then sbs=1b else sbs=0b
sbs_old=sbs

;h2=h/2. & h6=h/6.
i=0L
repeat begin

    h=h00*y[n_elements(y[0])-1]*1.5
    h2=h/2. & h6=h/6.

    sgn_pred=sgn
    yt=y+h2*dydx
    deriv,yt,dyt,bi,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
    yt=y+h2*dyt
    DERIV,yt,dym,bi,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
    yt=y+h*dym
    dym=dym+dyt
    DERIV,yt,dyt,bi,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
    y=y+h6*(dydx+dyt+2.*dym)
    DERIV,y,dydx,b1,bs,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
    b=[b,b1] & bv=[[bv(*,0),bs(0)],[bv(*,1),bs(1)],[bv(*,2),bs(2)]]
;    printf,lun,format='(i2,7f10.5)',i,y,dydx,b
    rtp=[[rtp(*,0),y(0)],[rtp(*,1),y(1)],[rtp(*,2),y(2)]]
    drtp=[[drtp(*,0),dydx(0)],[drtp(*,1),dydx(1)],[drtp(*,2),dydx(2)]]
    DERIV2,y,gbtmp,mfl_model=mfl_model,lsh=lsh,msh=msh,nocrt=nocrt,dip_param=dip_param
    gb=[[gb[*,0],gbtmp[0]],[gb[*,1],gbtmp[1]],[gb[*,2],gbtmp[2]]]
    arret=g_stop(y,mfl_model)
    nnn=n_elements(b)

    if keyword_set(lat_stop) then begin
        if l gt 0. then if y(1) ge lat_stop then arret=0.
        if l lt 0. then if y(1) le lat_stop then arret=0.
        if keyword_set(bmin) then if b1 lt bmin then arret=0.
        ; if y[0] gt 15. then arret=0.
    	;if nnn ge 2 then begin
    	;   if b[nnn-1] gt b[nnn-2] then arret=0.
    	;   if bs[0] ge 0 then sbs=1b else sbs=0b
    	;   if sbs ne sbs_old then arret=0.
        ; 	sbs_old=sbs
    	;endif
    endif

    if abs(arret) gt eps then sgn=arret/abs(arret) else goto,suite

    ;print,y(0)
endrep until sgn ne sgn_pred
suite:
;print,b1,2.8*b1,y,360-sls
x=rtp(*,0)*sin(rtp(*,1)*!pi/180.)*cos(rtp(*,2)*!pi/180.)
y=rtp(*,0)*sin(rtp(*,1)*!pi/180.)*sin(rtp(*,2)*!pi/180.)
z=rtp(*,0)*cos(rtp(*,1)*!pi/180.)
;drtp=drtp*h
;window,2,xsize=400,ysiz=400
;plot,sqrt(x^2+y^2),rtp(*,2)*!dtor,/polar
;window,1,xsize=400,ysiz=400
;plot,sqrt(x^2+y^2),z
return
end
