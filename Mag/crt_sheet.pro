;+
; Contains the crt_sheet procedure
;
; :Author:
;	 Baptiste Cecconi
;-
;
;+
; The purpose of this programme is to calculate the magnetic field due to the current disc. For this you need :
;
; 1. Transform the geographical spherical coordinates into magnetic cylindrical coordinates.
;
; 2. calculate the field
;
; 3. transform the field given in magnetic cylindrical coordinates into the field given in geographical spherical coordinates.
;
; Steps 1 and 3 are carried out through the intermediary of cartesian coordinates.
;
; :Params:
;	 r1:	in, required, type=sometype
;		 Radius in spherical coordinate
;	 t1:	in, required, type=sometype
;		 Theta in spherical coordinate
;	 p1:	in, required, type=sometype
;		 Phi in spherical coordinate
;	 crt:  in, required, type=sometype
;		 Current sheet parameters
;	 t:	 in, required, type=sometype
;		 Change matrix
;	 bres: out, required, type=sometype
;		 Field values
;
; :Keywords:
;	 test: in, optional, type=sometype
;		 A keyword named test
;-
pro CRT_SHEET,r1,t1,p1,crt,t,bres,test=test


nn = n_elements(r1)
tr=dblarr(3,3,nn) 
tr(0,0,*)=sin(double(t1))*cos(double(p1))
tr(1,0,*)=sin(double(t1))*sin(double(p1))
tr(2,0,*)=cos(double(t1))
tr(0,1,*)=cos(double(t1))*cos(double(p1))
tr(1,1,*)=cos(double(t1))*sin(double(p1))
tr(2,1,*)=-sin(double(t1))
tr(0,2,*)=-sin(double(p1))
tr(1,2,*)=cos(double(p1))
tr(2,2,*)=0.

if nn gt 1 then invtr = transpose(tr,[1,0,2]) else invtr = transpose(tr)
; tr is a rotation matrix, so invert==transpose
invt  = transpose(double(t))

;# Rotation matrix to be in magnetic cartesian coordinates
ttr = reform(t#reform(tr,3,3l*nn),3,3,nn) 
rr1 = rebin(reform(double(r1),1,nn),3,nn)
res = reform(ttr(*,0,*)*rr1,3,nn)

;# rho of the cylindrical coordinate. Useful to test if we are inside/outside the current sheet.
r2=sqrt(res(0,*)^2.+res(1,*)^2.) 

crt = double(crt)

;for i=0l,nn-1l do begin
;  print,r2(i),res(2,i)
;  ;print,ttr(*,*,i)
;endfor

;# Precalculation of Br et Bz to taking into account the finite nature of current sheet (by subtracting the field values, using small rho approximation for a semi-infinite sheet)
	f1=sqrt((res(2,*)-crt(2))^2+crt(1)^2)
	f2=sqrt((res(2,*)+crt(2))^2+crt(1)^2)
	brp2=r2/2.*(1./f1-1./f2)
	bzp2=2*crt(2)/sqrt(res(2,*)^2.+crt(1)^2.)- $
	r2^2/4.*((res(2,*)-crt(2))/f1^3.-(res(2,*)+crt(2))/f2^3.)

br2 = brp2*0.d0
bz2 = bzp2*0.d0

if keyword_set(test) then print,crt(0)
if keyword_set(test) then print,r2
if keyword_set(test) then print,crt(2)
if keyword_set(test) then print,abs(res(2,*))

;# The analytical equations for Brho and Bz vary depending on the region with respect to the current disk
;# see equations A1 through A9 from Connerney et al., 1981 https://doi.org/10.1029/JA086iA10p08370 
;# each time brp2*crt(3) or bzp2*crt(3) is subtracted to taking into account the finite nature of current sheet (with crt(3) is the mu0i0/2 term == current sheet current density) 

;# case where we are between the planet and the current sheet
w1 = where(r2 lt crt(0),cnt1)
if cnt1 ne 0 then begin
if keyword_set(test) then print,"case 1",w1
	f1(w1)=sqrt((res(2,w1)-crt(2))^2+crt(0)^2)
	f2(w1)=sqrt((res(2,w1)+crt(2))^2+crt(0)^2)
	br2(w1)=crt(3)*(r2(w1)/2.*(1./f1(w1)-1./f2(w1))-brp2(w1))
	bz2(w1)=crt(3)*(2*crt(2)/sqrt(res(2,w1)^2.+crt(0)^2.)- $
			 r2(w1)^2/4.*((res(2,w1)-crt(2))/f1(w1)^3.-(res(2,w1)+ $
			 crt(2))/f2(w1)^3.)-bzp2(w1))
endif

;# case where we are outside the current sheet (above or under)
w2 = where(r2 ge crt(0) and abs(res(2,*)) gt crt(2), cnt2)
if cnt2 ne 0 then begin
if keyword_set(test) then print,"case 2",w2
	f1(w2)=sqrt((res(2,w2)-crt(2))^2+r2(w2)^2)
	f2(w2)=sqrt((res(2,w2)+crt(2))^2+r2(w2)^2)
	br2(w2)=crt(3)*(((f1(w2)-f2(w2)+abs(res(2,w2))/res(2,w2)*2*crt(2))/r2(w2)-$ 
			 crt(0)^2*r2(w2)/4.*(1./f1(w2)^3-1./f2(w2)^3))-brp2(w2))
	bz2(w2)=crt(3)*(2*crt(2)/sqrt(res(2,w2)^2.+r2(w2)^2.)- $
			 crt(0)^2/4.*((res(2,w2)-crt(2))/f1(w2)^3.-(res(2,w2)+crt(2))/f2(w2)^3.)-bzp2(w2))
endif

;# case where we are inside the current sheet
w3 = where(r2 ge crt(0) and abs(res(2,*)) le crt(2), cnt3)
if cnt3 ne 0 then begin
if keyword_set(test) then print,"case 3",w3
	f1(w3)=sqrt((res(2,w3)-crt(2))^2+r2(w3)^2)
	f2(w3)=sqrt((res(2,w3)+crt(2))^2+r2(w3)^2)
	br2(w3)=crt(3)*(((f1(w3)-f2(w3)+2.*res(2,w3))/r2(w3)-crt(0)^2*r2(w3)/4.*(1./f1(w3)^3-1./f2(w3)^3))-brp2(w3))
	bz2(w3)=crt(3)*(2*crt(2)/sqrt(res(2,w3)^2.+r2(w3)^2.)- $
	crt(0)^2/4.*((res(2,w3)-crt(2))/f1(w3)^3.-(res(2,w3)+crt(2))/f2(w3)^3.)-bzp2(w3))
endif


;#=======
;#New to Jupiter with CAN2020 (not included in CAN1981): radial current produces an azimuthal field, so Bphi is nonzero
;#=======
if n_elements(crt) eq 5 then begin
	bphi2 = -2.7975d*crt[4]/r2
	w=where(abs(res[2,*]) lt crt[2],cnt)
	if cnt ne 0 then bphi2[w] = bphi2[w]*abs(res[2,w])/crt[2]
	w=where(res[2,*] gt 0.,cnt)
	if cnt ne 0 then bphi2[w] = (-1.d)*bphi2[w]
endif else bphi2 = 0

phi1=2*atan(res[1,*]/(sqrt(res[0,*]^2+res[1,*]^2)+res[0,*]))
;# before Connerney2020 Current Sheet
;# bres = [br2*res(0,*)/r2,br2*res(1,*)/r2,bz2]
bres = [br2*cos(phi1)-bphi2*sin(phi1),br2*sin(phi1)+bphi2*cos(phi1),bz2]





for i=0l,nn-1l do bres(*,i) = invtr(*,*,i)#invt#bres(*,i)
return
end
