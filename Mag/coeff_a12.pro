pro COEFF_A12, g,h,n,dip,crt,tdip
; Modele de champ Mercure MMMAnderson : Anderson et al. [2011,2012]

G=dblarr(6,6) & H=dblarr(6,6)
n=4
G(1,0)= -190e-5
G(1,1)= -190e-5 &  H(1,1)= 0

G(2,0)= -75e-5
G(2,1)= -21.5e-5 &  H(2,1)= 0.0
G(2,2)=  0.0 &  H(2,2)= 0.0

G(3,0)= -22e-5
G(3,1)= -2.98e-5 &  H(3,1)= 0.0

G(4,0)= -5.7e-5




;# Tilt in degres, longitude of the tilt
dip=[0.,0.]    

; Current sheet : 
crt=[0.,0.,0.,0.]	 ; current disc parameters: R_cs_min, R_cs_max, 
			         ; half-thick in planetary Radius, mu0*Itot/2/dRs in nT


tdip=[[1.,0.,0.],$	; passage matrix (coordinates changing)
      [0.,1.,0.],$
      [0.,0.,1.]]



return
end
