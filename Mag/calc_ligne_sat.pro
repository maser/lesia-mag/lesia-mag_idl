pro ROTATE_MAG,p,lg,lat,theta,longi,d_t,d_p


p0=(90.-p)*!dtor
lg0=(360.-lg)*!dtor

x0=sin(p0)*cos(lg0)
y0=sin(p0)*sin(lg0)
z0=cos(p0)

;print,"LA",lg,p0,lg0,x0,y0,z0
;rot xy

x1=x0*cos(d_p)-y0*sin(d_p)
y1=y0*cos(d_p)+x0*sin(d_p)
z1=z0

;rot,xz
x2=x1*cos(d_t)+z1*sin(d_t)
y2=y1
z2=z1*cos(d_t)-x1*sin(d_t)



;XYZ_TO_RTP, x2,y2,z2, r,theta,phi
theta=atan(sqrt(x2^2+y2^2),z2)
phi=atan(y2,x2)-d_p

theta=theta*!radeg
lat=90.-theta
longi=(360.-phi*!radeg) mod 360.
return
end

;-------------------------------------------------------------------------------
PRO CONVERT,brtp,bxyz,xyz,n
;-------------------------------------------------------------------------------
; S. Hess (2006), L. Lamy (10/2007)
;-------------------------------------------------------------------------------


bxyz=brtp*0.
r=xyz(*,0)
theta=xyz(*,1)
phi=xyz(*,2)
bxyz(0,*)=sin(theta)*cos(phi)*brtp(0,*)+cos(theta)*cos(phi)*brtp(1,*)-sin(phi)*brtp(2,*)
bxyz(1,*)=sin(theta)*sin(phi)*brtp(0,*)+cos(theta)*sin(phi)*brtp(1,*)+cos(phi)*brtp(2,*)
bxyz(2,*)=cos(theta)*brtp(0,*)-sin(theta)*brtp(1,*)

return
end

;-------------------------------------------------------------------------------
PRO CALC_LIGNE_SAT
;-------------------------------------------------------------------------------
; p1,p2 = bornes en colatitude des lignes de champ calcul�es (origine=pole nord)
; dossier = chemin du dossier d'archivage des fichiers
; /conj = permet de calculer la ligne de champ conjugu�e dans l'h�misph�re oppos�
;         (stock�e dans le rep conjugate_mfl de dossier)
;
; S. Hess (2006), L. Lamy (10/2007)
;-------------------------------------------------------------------------------

dossier=''
models=['VIT4','VIP4','O6','VIPAL','ISAC','JRM09','SPV','Z3']
n_mod=n_elements(models)
eps=[0.064935,0.064935,0.064935,0.064935,0.064935,0.064935,0.098,0.098]
Radiu=[71490000.,71490000.,71490000.,71490000.,71490000.,71490000.,60268000.,60268000.]
axi=[0b,0b,0b,0b,0b,0b,1b,1b]



for c_model=5,5 do begin

Rp=Radiu[c_model]
c=299792458.0
c2=c^2
fsb=2.79924835996
e=eps[c_model]
mfl_model=models[c_model]
print,mfl_model
COEFF,g,h,n,dip,crt,tdip,mfl_model=mfl_model

mag_step=0.001*fsb*(sqrt(g(1,0)^2+g(1,1)^2+h(1,1)^2)/(1.-e)^3)
magstep=mag_step

dip_theta=atan(sqrt(g(1,1)^2+h(1,1)^2),g(1,0))
dip_phi=atan(h(1,1),g(1,1))

;(1) lat

p1=2.
p2=0.
for p=p1,p2,1 do begin

lgmax=359
if (axi[c_model]) then lgmax=0 else begin
cmd='mkdir '+models[c_model]+'_lat/'+STRSPLIT(STRING(fix(p)),/EXTRACT)
spawn,cmd,resu
print,"trying ",cmd," got",resu
endelse

for lg=1,lgmax do begin
  ROTATE_MAG,p,lg,lat,theta,longi,dip_theta,dip_phi
  l=1/sqrt((cos(lat*!dtor))^2+(sin(lat*!dtor)/(1-e))^2)*1.0001
print, p,lg,lat,theta,longi,dip_theta,dip_phi
;if lg eq 90 then stop
  L_SHELL_TEST_N,-l,longi,theta,rtp,x,y,z,B,bmod,gb,mag_step,mfl_model=mfl_model

stop,'STOP : supprimer re-ordonnement par valeur de B et supprimer echantillonnage  (idem correction lsh plus bas)'
srt=sort(bmod)
bmod=bmod(srt)
b=b[srt,*]
gb=gb[srt,*]
x=x[srt]
y=y[srt]
z=z[srt]
rtp=rtp[srt,*]

  bmod=bmod*fsb
  b=b*fsb
  b=transpose(b)
  gb=transpose(gb)
  sc_gb=sqrt(total(gb^2,1))
  gb[0,*]/=sc_gb
  gb[1,*]/=sc_gb
  gb[2,*]/=sc_gb
  sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)

nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
mff=max(bmod)-nff*mag_step
ff=findgen(nff+1)*mag_step+mff
b_new=dblarr(3,nff+1)
rtp_new=dblarr(nff+1,3)
gb_new=fltarr(3,nff+1)
x= interpol(x,bmod,ff)
y= interpol(y,bmod,ff)
z= interpol(z,bmod,ff)
sc_gb= interpol(sc_gb,bmod,ff)
gb_new[0,*] = interpol(gb[0,*],bmod,ff)
gb_new[1,*] = interpol(gb[1,*],bmod,ff)
gb_new[2,*] = interpol(gb[2,*],bmod,ff)
gb=gb_new
b_new[0,*] = interpol(b[0,*],bmod,ff)
b_new[1,*] = interpol(b[1,*],bmod,ff)
b_new[2,*] = interpol(b[2,*],bmod,ff)
b=b_new
rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
rtp=rtp_new

bmod=ff

  n=n_elements(bmod)
  ; Conversion rtp->xyz et normalisation
  rtp(*,1:2)=rtp(*,1:2)*!dtor
  bz=rebin([1.,0.,0.],3,n)

  b(0,*)/=bmod
  b(1,*)/=bmod
  b(2,*)/=bmod
  convert,b,b2,rtp,n
  b=b2
  convert,bz,b2,rtp,n
  bz=b2
  convert,gb,b2,rtp,n
  gb=b2
  
  bz=bz-total(bz*b)*b
  bz=bz/sqrt(total(bz^2))

  gbz=total(gb*bz,1)
  gbb=total(gb*b,1)
  gb=atan(gbz,gbb)
print,"lat ",p," long ",lg," North n",n,' fmax ',max(bmod)
x=float(x)
y=float(y)
z=float(z)
b=float(b)
bz=float(bz)
bmod=float(bmod)
gb=float(gb)
sc_gb=float(sc_gb)  

if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lat/'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lat/'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
  openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
  writeu,unit,n
  writeu,unit,x
  writeu,unit,y
  writeu,unit,z
  writeu,unit,b
  writeu,unit,bmod
  writeu,unit,sc_gb
  writeu,unit,bz
  writeu,unit,gb
  close, unit & free_lun, unit

  ROTATE_MAG,-p,lg,lat,theta,longi,dip_theta,dip_phi
print, p,lg,lat,theta,longi,dip_theta,dip_phi
  l=1/sqrt((cos(lat*!dtor))^2+(sin(lat*!dtor)/(1-e))^2)*1.0001
  
  L_SHELL_TEST_N,l,longi,theta,rtp,x,y,z,B,bmod,gb,mag_step/fsb,mfl_model=mfl_model

srt=sort(bmod)
bmod=bmod(srt)
b=b[srt,*]
gb=gb[srt,*]
x=x[srt]
y=y[srt]
z=z[srt]
rtp=rtp[srt,*]

  bmod=bmod*fsb
  b=b*fsb
  b=transpose(b)
  gb=transpose(gb)
  sc_gb=sqrt(total(gb^2,1))
  gb[0,*]/=sc_gb
  gb[1,*]/=sc_gb
  gb[2,*]/=sc_gb
  sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)

nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
mff=max(bmod)-nff*mag_step
ff=findgen(nff+1)*mag_step+mff
b_new=dblarr(3,nff+1)
rtp_new=dblarr(nff+1,3)
gb_new=fltarr(3,nff+1)
x= interpol(x,bmod,ff)
y= interpol(y,bmod,ff)
z= interpol(z,bmod,ff)
sc_gb= interpol(sc_gb,bmod,ff)
gb_new[0,*] = interpol(gb[0,*],bmod,ff)
gb_new[1,*] = interpol(gb[1,*],bmod,ff)
gb_new[2,*] = interpol(gb[2,*],bmod,ff)
gb=gb_new
b_new[0,*] = interpol(b[0,*],bmod,ff)
b_new[1,*] = interpol(b[1,*],bmod,ff)
b_new[2,*] = interpol(b[2,*],bmod,ff)
b=b_new
rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
rtp=rtp_new


bmod=ff
  n=n_elements(bmod)

  ; Conversion rtp->xyz et normalisation
  rtp(*,1:2)=rtp(*,1:2)*!dtor
  bz=rebin([1.,0.,0.],3,n)

  b(0,*)/=bmod
  b(1,*)/=bmod
  b(2,*)/=bmod
  convert,b,b2,rtp,n
  b=b2
  convert,bz,b2,rtp,n
  bz=b2
  convert,gb,b2,rtp,n
  gb=b2
  
  bz=bz-total(bz*b)*b
  bz=bz/sqrt(total(bz^2))

  gbz=total(gb*bz,1)
  gbb=total(gb*b,1)
  gb=atan(gbz,gbb)

print,"lat ",p," long ",lg," South n",n,' fmax ',max(bmod)

x=float(x)
y=float(y)
z=float(z)
b=float(b)
bz=float(bz)
bmod=float(bmod)
sc_gb=float(sc_gb)

if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lat/-'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lat/'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
  openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
  writeu,unit,n
  writeu,unit,x
  writeu,unit,y
  writeu,unit,z
  writeu,unit,b
  writeu,unit,bmod
  writeu,unit,sc_gb
  writeu,unit,bz
  writeu,unit,gb
  close, unit & free_lun, unit
endfor
endfor


;(2) lsh

;### Io ###
;p1=5.95
;p2=5.95
;moon='Io'
;### Europa ###
;p1=9.5
;p2=9.5
;moon='Europa'
;#########
p1=2
p2=90

for p=p1,p2,1 do begin
lgmax=359
if (axi[c_model]) then lgmax=0 else begin
if keyword_set(moon) then cmd='mkdir '+models[c_model]+'_lsh/'+moon else $
  cmd='mkdir '+models[c_model]+'_lsh/'+STRSPLIT(STRING(fix(p)),/EXTRACT)

spawn,cmd,resu
print,"trying ",cmd," got",resu
endelse

for lg=0,lgmax do begin
  L_SHELL_TEST_N,p,lg,90.,rtp,x,y,z,B,bmod,gb,mag_step,mfl_model=mfl_model 
; LL 21/07/15 : r�ordonnement pose probl�me quand la variation de bmod n'est pas d�croissante
;               Ex : Io nord, longi = 50�
;srt=sort(bmod)
;bmod=bmod(srt)
;b=b[srt,*]
;gb=gb[srt,*]
;x=x[srt]
;y=y[srt]
;z=z[srt]
;rtp=rtp[srt,*]
  bmod=bmod*fsb
  b=b*fsb
  b=transpose(b)
  gb=transpose(gb)
  sc_gb=sqrt(total(gb^2,1))
  gb[0,*]/=sc_gb
  gb[1,*]/=sc_gb
  gb[2,*]/=sc_gb
  sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)

; LL 21/07/15 : r�interpolation pose probl�me quand la variation de bmod n'est pas d�croissante, idem plus haut
;               r�-�chantillonage supprim� pour le moment
;nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
;mff=max(bmod)-nff*mag_step
;ff=findgen(nff+1)*mag_step+mff
;b_new=dblarr(3,nff+1)
;rtp_new=dblarr(nff+1,3)
;gb_new=fltarr(3,nff+1)
;x= interpol(x,bmod,ff)
;y= interpol(y,bmod,ff)
;z= interpol(z,bmod,ff)
;sc_gb= interpol(sc_gb,bmod,ff)
;gb_new[0,*] = interpol(gb[0,*],bmod,ff)
;gb_new[1,*] = interpol(gb[1,*],bmod,ff)
;gb_new[2,*] = interpol(gb[2,*],bmod,ff)
;gb=gb_new
;b_new[0,*] = interpol(b[0,*],bmod,ff)
;b_new[1,*] = interpol(b[1,*],bmod,ff)
;b_new[2,*] = interpol(b[2,*],bmod,ff)
;b=b_new
;rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
;rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
;rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
;rtp=rtp_new
;bmod=ff

  n=n_elements(bmod)
  ; Conversion rtp->xyz et normalisation
  rtp(*,1:2)=rtp(*,1:2)*!dtor
  bz=rebin([1.,0.,0.],3,n)

  b(0,*)/=bmod
  b(1,*)/=bmod
  b(2,*)/=bmod
  convert,b,b2,rtp,n
  b=b2
  convert,bz,b2,rtp,n
  bz=b2
  convert,gb,b2,rtp,n
  gb=b2
  
  bz=bz-total(bz*b)*b
  bz=bz/sqrt(total(bz^2))

  gbz=total(gb*bz,1)
  gbb=total(gb*b,1)
  gb=atan(gbz,gbb)
print,"dist ",p," long ",lg," North n",n,' fmax ',max(bmod)
x=float(x)
y=float(y)
z=float(z)
b=float(b)
bz=float(bz)
bmod=float(bmod)
gb=float(gb)
sc_gb=float(sc_gb)  

if keyword_set(moon) then if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh/'+moon+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh/'+moon+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr' else $
  if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh/'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh/'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'

  openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
  writeu,unit,n
  writeu,unit,x
  writeu,unit,y
  writeu,unit,z
  writeu,unit,b
  writeu,unit,bmod
  writeu,unit,sc_gb
  writeu,unit,bz
  writeu,unit,gb
  close, unit & free_lun, unit
  
  L_SHELL_TEST_N,-p,lg,90.,rtp,x,y,z,B,bmod,gb,mag_step/fsb,mfl_model=mfl_model

;srt=sort(bmod)
;bmod=bmod(srt)
;b=b[srt,*]
;gb=gb[srt,*]
;x=x[srt]
;y=y[srt]
;z=z[srt]
;rtp=rtp[srt,*]

  bmod=bmod*fsb
  b=b*fsb
  b=transpose(b)
  gb=transpose(gb)
  sc_gb=sqrt(total(gb^2,1))
  gb[0,*]/=sc_gb
  gb[1,*]/=sc_gb
  gb[2,*]/=sc_gb
  sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)

; LL 21/07/15 : r�interpolation pose probl�me quand la variation de bmod n'est pas d�croissante, idem plus haut
;               r�-�chantillonage supprim� pour le moment
;nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
;mff=max(bmod)-nff*mag_step
;ff=findgen(nff+1)*mag_step+mff-mag_step/2. 
;b_new=dblarr(3,nff+1)
;rtp_new=dblarr(nff+1,3)
;gb_new=fltarr(3,nff+1)
;x= interpol(x,bmod,ff)
;y= interpol(y,bmod,ff)
;z= interpol(z,bmod,ff)
;sc_gb= interpol(sc_gb,bmod,ff)
;gb_new[0,*] = interpol(gb[0,*],bmod,ff)
;gb_new[1,*] = interpol(gb[1,*],bmod,ff)
;gb_new[2,*] = interpol(gb[2,*],bmod,ff)
;gb=gb_new
;b_new[0,*] = interpol(b[0,*],bmod,ff)
;b_new[1,*] = interpol(b[1,*],bmod,ff)
;b_new[2,*] = interpol(b[2,*],bmod,ff)
;b=b_new
;rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
;rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
;rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
;rtp=rtp_new
; bmod=ff

  n=n_elements(bmod)

  ; Conversion rtp->xyz et normalisation
  rtp(*,1:2)=rtp(*,1:2)*!dtor
  bz=rebin([1.,0.,0.],3,n)

  b(0,*)/=bmod
  b(1,*)/=bmod
  b(2,*)/=bmod
  convert,b,b2,rtp,n
  b=b2
  convert,bz,b2,rtp,n
  bz=b2
  convert,gb,b2,rtp,n
  gb=b2
  
  bz=bz-total(bz*b)*b
  bz=bz/sqrt(total(bz^2))

  gbz=total(gb*bz,1)
  gbb=total(gb*b,1)
  gb=atan(gbz,gbb)

print,"dist ",p," long ",lg," South n",n,' fmax ',max(bmod)

x=float(x)
y=float(y)
z=float(z)
b=float(b)
bz=float(bz)
bmod=float(bmod)
gb=float(gb)
sc_gb=float(sc_gb)

if keyword_set(moon) then if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh/-'+moon+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh/'+moon+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr' else $
  if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh/-'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh/'+STRSPLIT(STRING(fix(p)),/EXTRACT)+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
  openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
  writeu,unit,n
  writeu,unit,x
  writeu,unit,y
  writeu,unit,z
  writeu,unit,b
  writeu,unit,bmod
  writeu,unit,sc_gb
  writeu,unit,bz
  writeu,unit,gb
  close, unit & free_lun, unit
endfor
endfor


endfor

return
end
