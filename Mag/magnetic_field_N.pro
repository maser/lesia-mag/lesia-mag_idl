function P,L,M,H,lsh=lsh
if keyword_set(lsh) then return,LEGENDRE(H,L,M)
return,[SQRT((1.+(M<1))*(factorial(L-M))/(factorial(L+M)))*LEGENDRE(H,L,M)*(-1.)^M]
end

pro MAGNETIC_FIELD_N,R, THETA, PHI, B_SPH,B,br,g,h,mfl_model=mfl_model,nocrt=nocrt,lsh=lsh,msh=msh,dip_param=dip_param
b_sph=dblarr(3)
rtheta=theta*!dpi/180.d0
rphi=phi*!dpi/180.d0
if keyword_set(dip_param) then begin
	g=dip_param.g
	h=dip_param.h
	N=dip_param.n
	dip=dip_param.dip
	crt=dip_param.crt
	tdip=dip_param.tdip
endif else COEFF,g,h,n,dip,crt,tdip,mfl_model=mfl_model
;n=10
c=cos(dindgen(N+1)*rphi)
s=sin(dindgen(N+1)*rphi)
ct=cos(rtheta)
ctp=cos(rtheta+0.0001)

for i=1,N do begin
RP=1.d0/r^(i+2)
j=indgen(i+1)

PIJ=P(i,j,ct,lsh=lsh)*RP
DIJ=(P(i,j,ctp,lsh=lsh)*RP-PIJ)*10000.d0
b_sph(0)=b_sph(0)+double(i+1)*total(PIJ*(G[i,j]*C[j]+H[i,j]*S[j]))
b_sph(1)=b_sph(1)-total(DIJ*(G[i,j]*C[j]+H[i,j]*S[j]))
b_sph(2)=b_sph(2)+total(PIJ*j*(G[i,j]*S[j]-H[i,j]*C[j]))
endfor
b_sph(2)=b_sph(2)/sin(rtheta)

if not(keyword_set(nocrt)) then CRT_SHEET,r,rtheta,rphi,crt,tdip,br else br=0.
b_sph=b_sph+br*1.e-5
b=sqrt(total(b_sph^2.))
return
end
