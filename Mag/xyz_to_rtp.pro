; --------------------------------------
  pro XYZ_TO_RTP, x,y,z, r,theta,phi; --------------------------------------
; x,y,z & r in Rj; theta, phi in rad
  x=x*1. & y=y*1. & z=z*1.  i=where(x eq 0.) & if i(0) ne -1 then x(i)=0.000001  i=where(y eq 0.) & if i(0) ne -1 then y(i)=0.000001  r=sqrt(x^2+y^2+z^2)  theta=!pi/2.-atan(z/sqrt(x^2+y^2))  phi=atan(y/x)  i=where(x lt 0 and y gt 0) & if i(0) ne -1 then phi(i)=phi(i)+!pi  i=where(x lt 0 and y lt 0) & if i(0) ne -1 then phi(i)=phi(i)+!pi  i=where(x gt 0 and y lt 0) & if i(0) ne -1 then phi(i)=phi(i)+2*!pireturnend