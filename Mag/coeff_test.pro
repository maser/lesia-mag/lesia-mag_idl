pro COEFF_TEST, g,h,n,dip,crt,tdip

; coefficients modele test DIPOLE + tilt

G=dblarr(6,6) & H=dblarr(6,6)
n=1
G(1,0)=1.
G(1,1)=0.  &  H(1,1)=1.

dip=[0.,0.]	; tilt en degres, longitude du pole N magnetique en degres

crt=[8.,16.,2.5,0]	; parametres du disque de courant: Rsmin, Rsmax, 
			; 1/2 epaisseur en Rs, mu0*Itot/2/dRs en nT

tdip=[[1.,0.,0.],$	; matrice de changement de coordonnees
      [0.,1.,0.],$
      [0.,0.,1.]]

return
end
