pro ROTATE_MAG,p,lg,lat,theta,longi,d_t,d_p


p0=(90.-p)*!dtor
lg0=(360.-lg)*!dtor

x0=sin(p0)*cos(lg0)
y0=sin(p0)*sin(lg0)
z0=cos(p0)

;print,"LA",lg,p0,lg0,x0,y0,z0
;rot xy

x1=x0*cos(d_p)-y0*sin(d_p)
y1=y0*cos(d_p)+x0*sin(d_p)
z1=z0

;rot,xz
x2=x1*cos(d_t)+z1*sin(d_t)
y2=y1
z2=z1*cos(d_t)-x1*sin(d_t)



;XYZ_TO_RTP, x2,y2,z2, r,theta,phi
theta=atan(sqrt(x2^2+y2^2),z2)
phi=atan(y2,x2)-d_p

theta=theta*!radeg
lat=90.-theta
longi=(360.-phi*!radeg) mod 360.
return
end

;-------------------------------------------------------------------------------
PRO CONVERT,brtp,bxyz,xyz,n
;-------------------------------------------------------------------------------
; S. Hess (2006), L. Lamy (10/2007)
;-------------------------------------------------------------------------------


bxyz=brtp*0.
r=xyz(*,0)
theta=xyz(*,1)
phi=xyz(*,2)
bxyz(0,*)=sin(theta)*cos(phi)*brtp(0,*)+cos(theta)*cos(phi)*brtp(1,*)-sin(phi)*brtp(2,*)
bxyz(1,*)=sin(theta)*sin(phi)*brtp(0,*)+cos(theta)*sin(phi)*brtp(1,*)+cos(phi)*brtp(2,*)
bxyz(2,*)=cos(theta)*brtp(0,*)-sin(theta)*brtp(1,*)

return
end

;-------------------------------------------------------------------------------
PRO CALC_MFL,l1=l1,l2=l2,l_step=l_step,$
        lIIIWest1 = lIIIWest1, lIIIWest2 = lIIIWest2,$
        msh=msh,lat=lat,lsh=lsh,moon=moon, $
        mfl_model=mfl_model,nocrt=nocrt,$
        test=test
;-------------------------------------------------------------------------------

; if keyword_set(lat): Calculation of the magnetic field lines using Colatitude (origin=North pole).
; if keyword_set(lsh): Calculation of the magnetic field lines using apex (=distance in plantari radius) along the planetocentric equator.
; if keyword_set(msh): Calculation of the magnetic field lines using apex (=distance in plantari radius) along the magnetic equator.
; l1,l2,l_step (INPUT): boundaries of colatitude or apex, and step for the calculation
; lIIIWest1, lIIIWest2 (INPUT): boundaries of longitude (in System III, counted West) for the calculation (default value: 0 to 359)
; mfl_model: magnetic field model for used for the calculation
; nocrt: to not take into account a current sheet model
; test: to create the file in a subdirectory named "test"
; S. Hess (2006), L. Lamy (10/2007)
;-------------------------------------------------------------------------------

if ~keyword_set(mfl_model) then stop,"Please enter a magnetic field model with the keyword: mfl_model='****' "
if keyword_Set(test) then filenametest='_test' else filenametest=''
if ~keyword_set(moon) then begin
  if ~keyword_set(l1) then print,"Please enter a start apex (if /lsh o /msh) or latitude (if /lat) with the keyword: l1='**'"
  if ~keyword_set(l2) then print,"Please enter an end apex (if /lsh o /msh) or latitude (if /lat) with the keyword: l2='**'"
endif
if ~keyword_set(lIIIWest1) then lIIIWest1 = 0
if ~keyword_set(lIIIWest2) then lIIIWest2 = 359
if ~keyword_set(l_step) then l_step = 1
if ~keyword_set(lat) and ~keyword_set(lsh) and ~keyword_set(msh) then print, "Please enter a way to calculate the magnetic field lines (/lsh, /msh or /lat)"

dossier='../../'
models=['VIT4','VIP4','O6','VIPAL','ISaAC','JRM09','JRM09_CS2020','JRM33','SPV','Z3','A12','IGRF1995','IGRF2000', 'TDS', 'TDS_axi','proxima_centauri', 'Q3','AH5']
n_mod=n_elements(models)
eps=[0.064935,0.064935,0.064935,0.064935,0.064935,0.064935,0.064935,0.064935,0.098,0.098, 0.0000, 0.0033528, 0.0033528, 0, 0, 0, 0.02293,0.02293]
Radiu=[71490000.,71490000.,71490000.,71490000.,71490000.,71490000.,71490000.,71490000.,60268000.,60268000., 2439700., 6378100., 6378100 , 71490000., 71490000.,107137800., 25559000.,25559000.]
axi=[0b,0b,0b,0b,0b,0b,0b,0b,1b,1b,0b,0b,0b,0b,1b, 0b, 0b,0b]

wmodel=[]
for i=0,n_elements(mfl_model)-1 do $
  wmodel=[wmodel,where(strlowcase(models) eq strlowcase(mfl_model[i]))]

wmodel=wmodel[sort(wmodel)]

if wmodel[0] eq -1 then stop,'Please verify the magnetic field model name'
for i=0,n_elements(wmodel)-1 do begin

c_model=wmodel[i]
Rp=Radiu[c_model]
c=299792458.0
c2=c^2
fsb=2.79924835996
e=eps[c_model]
mfl_model=models[c_model]
print,mfl_model
COEFF,g,h,n,dip,crt,tdip,mfl_model=mfl_model

dipole_param={G:G,$
              H:H,$
              N:N,$
              dip:dip,$
              crt:crt,$
              tdip:tdip}

mag_step=0.001*fsb*(sqrt(g(1,0)^2+g(1,1)^2+h(1,1)^2)/(1.-e)^3)
magstep=mag_step
dip_theta=atan(sqrt(g(1,1)^2+h(1,1)^2),g(1,0))
dip_phi=atan(h(1,1),g(1,1))

if keyword_set(lat) then begin

  ;(1) lat
  
  for p=l1,l2,l_step do begin


    if (axi[c_model]) then lgmax=0 else begin
      if (fix(p)-p eq 0) then p_directory = fix(p) $
        else  p_directory = string(format = '(F4.2)',p) 
      cmd='mkdir '+dossier+models[c_model]+'_lat'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)
      spawn,cmd,resu
      print,"trying ",cmd," got",resu
    endelse

    for lg=lIIIWest1,lIIIWest2 do begin
      ROTATE_MAG,p,lg,lat,theta,longi,dip_theta,dip_phi
      l=1/sqrt((cos(lat*!dtor))^2+(sin(lat*!dtor)/(1-e))^2)*1.0001
      print, p,lg,lat,theta,longi,dip_theta,dip_phi
      ;if lg eq 90 then stop
      L_SHELL_TEST_N,-l,longi,theta,rtp,x,y,z,B,bmod,gb,mag_step,mfl_model=mfl_model,dip_param=dip_param
  
      ;stop,'STOP : supprimer re-ordonnement par valeur de B et supprimer echantillonnage  (idem correction lsh plus bas)'

      ;srt=sort(bmod)
      ;bmod=bmod(srt)
      ;b=b[srt,*]
      ;gb=gb[srt,*]
      ;x=x[srt]
      ;y=y[srt]
      ;z=z[srt]
      ;rtp=rtp[srt,*]
  
      bmod=bmod*fsb
      b=b*fsb
      b=transpose(b)
      gb=transpose(gb)
      sc_gb=sqrt(total(gb^2,1))
      gb[0,*]/=sc_gb
      gb[1,*]/=sc_gb
      gb[2,*]/=sc_gb
      sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)
  
      nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
      mff=max(bmod)-nff*mag_step
      ff=findgen(nff+1)*mag_step+mff
      b_new=dblarr(3,nff+1)
      rtp_new=dblarr(nff+1,3)
      gb_new=fltarr(3,nff+1)
      x= interpol(x,bmod,ff)
      y= interpol(y,bmod,ff)
      z= interpol(z,bmod,ff)
      sc_gb= interpol(sc_gb,bmod,ff)
      gb_new[0,*] = interpol(gb[0,*],bmod,ff)
      gb_new[1,*] = interpol(gb[1,*],bmod,ff)
      gb_new[2,*] = interpol(gb[2,*],bmod,ff)
      gb=gb_new
      b_new[0,*] = interpol(b[0,*],bmod,ff)
      b_new[1,*] = interpol(b[1,*],bmod,ff)
      b_new[2,*] = interpol(b[2,*],bmod,ff)
      b=b_new
      rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
      rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
      rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
      rtp=rtp_new
  
      bmod=ff
  
      n=n_elements(bmod)
      ; Conversion rtp->xyz et normalisation
      rtp(*,1:2)=rtp(*,1:2)*!dtor
      bz=rebin([1.,0.,0.],3,n)
    
      b(0,*)/=bmod
      b(1,*)/=bmod
      b(2,*)/=bmod
      convert,b,b2,rtp,n
      b=b2
      convert,bz,b2,rtp,n
      bz=b2
      convert,gb,b2,rtp,n
      gb=b2
      
      bz=bz-total(bz*b)*b
      bz=bz/sqrt(total(bz^2))
    
      gbz=total(gb*bz,1)
      gbb=total(gb*b,1)
      gb=atan(gbz,gbb)
      print,"lat ",p," long ",lg," North n",n,' fmax ',max(bmod)
      x=float(x)
      y=float(y)
      z=float(z)
      b=float(b)
      bz=float(bz)
      bmod=float(bmod)
      gb=float(gb)
      sc_gb=float(sc_gb)  
  
      if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lat/'+STRSPLIT(p_directory,/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lat/'+STRSPLIT(p_directory,/EXTRACT)+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
      openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
      writeu,unit,n
      writeu,unit,x
      writeu,unit,y
      writeu,unit,z
      writeu,unit,b
      writeu,unit,bmod
      writeu,unit,sc_gb
      writeu,unit,bz
      writeu,unit,gb
      close, unit & free_lun, unit
  
      ROTATE_MAG,-p,lg,lat,theta,longi,dip_theta,dip_phi
      print, p,lg,lat,theta,longi,dip_theta,dip_phi
      l=1/sqrt((cos(lat*!dtor))^2+(sin(lat*!dtor)/(1-e))^2)*1.0001
    
      L_SHELL_TEST_N,l,longi,theta,rtp,x,y,z,B,bmod,gb,mag_step/fsb,mfl_model=mfl_model,dip_param=dipole_param
  
     ; srt=sort(bmod)
     ; bmod=bmod(srt)
     ; b=b[srt,*]
     ; gb=gb[srt,*]
     ; x=x[srt]
     ; y=y[srt]
     ; z=z[srt]
     ; rtp=rtp[srt,*]
  
      bmod=bmod*fsb
      b=b*fsb
      b=transpose(b)
      gb=transpose(gb)
      sc_gb=sqrt(total(gb^2,1))
      gb[0,*]/=sc_gb
      gb[1,*]/=sc_gb
      gb[2,*]/=sc_gb
      sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)
  
      nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
      mff=max(bmod)-nff*mag_step
      ff=findgen(nff+1)*mag_step+mff
      b_new=dblarr(3,nff+1)
      rtp_new=dblarr(nff+1,3)
      gb_new=fltarr(3,nff+1)
      x= interpol(x,bmod,ff)
      y= interpol(y,bmod,ff)
      z= interpol(z,bmod,ff)
      sc_gb= interpol(sc_gb,bmod,ff)
      gb_new[0,*] = interpol(gb[0,*],bmod,ff)
      gb_new[1,*] = interpol(gb[1,*],bmod,ff)
      gb_new[2,*] = interpol(gb[2,*],bmod,ff)
      gb=gb_new
      b_new[0,*] = interpol(b[0,*],bmod,ff)
      b_new[1,*] = interpol(b[1,*],bmod,ff)
      b_new[2,*] = interpol(b[2,*],bmod,ff)
      b=b_new
      rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
      rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
      rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
      rtp=rtp_new
      
      
      bmod=ff
      n=n_elements(bmod)
  
      ; Conversion rtp->xyz et normalisation
      rtp(*,1:2)=rtp(*,1:2)*!dtor
      bz=rebin([1.,0.,0.],3,n)
  
      b(0,*)/=bmod
      b(1,*)/=bmod
      b(2,*)/=bmod
      convert,b,b2,rtp,n
      b=b2
      convert,bz,b2,rtp,n
      bz=b2
      convert,gb,b2,rtp,n
      gb=b2
      
      bz=bz-total(bz*b)*b
      bz=bz/sqrt(total(bz^2))
  
      gbz=total(gb*bz,1)
      gbb=total(gb*b,1)
      gb=atan(gbz,gbb)
  
      print,"lat ",p," long ",lg," South n",n,' fmax ',max(bmod)
      
      x=float(x)
      y=float(y)
      z=float(z)
      b=float(b)
      bz=float(bz)
      bmod=float(bmod)
      sc_gb=float(sc_gb)
      
      if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lat/-'+STRSPLIT(p_directory,/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lat/'+STRSPLIT(p_directory,/EXTRACT)+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
      openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
      writeu,unit,n
      writeu,unit,x
      writeu,unit,y
      writeu,unit,z
      writeu,unit,b
      writeu,unit,bmod
      writeu,unit,sc_gb
      writeu,unit,bz
      writeu,unit,gb
      close, unit & free_lun, unit
    endfor
  endfor
endif

if keyword_set(lsh) then begin

  ;(2) lsh
  
  if keyword_set(moon) then begin
    if moon eq 1 then begin
      moon_name=''
      READ, moon_name, PROMPT="Enter moon's name: "
      moon = moon_name
      READ, apex_moon, PROMPT="Enter apex (L shell) of the moon: "
      l1 = apex_moon
      l2 = apex_moon
    endif
    if strlowcase(moon) eq 'io' then begin
      ;### Io ###
      l1=5.95
      l2=5.95
    endif else if strlowcase(moon) eq 'europa' then begin
      ;### Europa ###
      l1=9.5
      l2=9.5
    endif else if strlowcase(moon) eq 'ganymede' then begin
      ;### Europa ###
      l1=15
      l2=15
    endif
  endif
  
  for p=l1,l2,l_step do begin

    if (axi[c_model]) then lgmax=0 else begin
    if (fix(p)-p eq 0) then p_directory = fix(p) $
      else  p_directory = string(format = '(F4.2)',p) 
      if keyword_set(moon) then cmd='mkdir '+dossier+models[c_model]+'_lsh'+filenametest+'/'+moon else $
        cmd='mkdir '+dossier+models[c_model]+'_lsh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)
    
      spawn,cmd,resu
      
      print,"trying ",cmd," got",resu
    endelse
    for lg=lIIIWest1,lIIIWest2 do begin
        L_SHELL_TEST_N,p,lg,90.,rtp,x,y,z,B,bmod,gb,mag_step,mfl_model=mfl_model,lsh=0,dip_param=dipole_param
      ; LL 21/07/15 : rŽordonnement pose problme quand la variation de bmod n'est pas dŽcroissante
      ;               Ex : Io nord, longi = 50¡
      ;srt=sort(bmod)
      ;bmod=bmod(srt)
      ;b=b[srt,*]
      ;gb=gb[srt,*]
      ;x=x[srt]
      ;y=y[srt]
      ;z=z[srt]
      ;rtp=rtp[srt,*]
        bmod=bmod*fsb
        b=b*fsb
        b=transpose(b)
        gb=transpose(gb)
        sc_gb=sqrt(total(gb^2,1))
        gb[0,*]/=sc_gb
        gb[1,*]/=sc_gb
        gb[2,*]/=sc_gb
        sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)
      
      ; LL 21/07/15 : rŽinterpolation pose problme quand la variation de bmod n'est pas dŽcroissante, idem plus haut
      ;               rŽ-Žchantillonage supprimŽ pour le moment
      ;nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
      ;mff=max(bmod)-nff*mag_step
      ;ff=findgen(nff+1)*mag_step+mff
      ;b_new=dblarr(3,nff+1)
      ;rtp_new=dblarr(nff+1,3)
      ;gb_new=fltarr(3,nff+1)
      ;x= interpol(x,bmod,ff)
      ;y= interpol(y,bmod,ff)
      ;z= interpol(z,bmod,ff)
      ;sc_gb= interpol(sc_gb,bmod,ff)
      ;gb_new[0,*] = interpol(gb[0,*],bmod,ff)
      ;gb_new[1,*] = interpol(gb[1,*],bmod,ff)
      ;gb_new[2,*] = interpol(gb[2,*],bmod,ff)
      ;gb=gb_new
      ;b_new[0,*] = interpol(b[0,*],bmod,ff)
      ;b_new[1,*] = interpol(b[1,*],bmod,ff)
      ;b_new[2,*] = interpol(b[2,*],bmod,ff)
      ;b=b_new
      ;rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
      ;rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
      ;rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
      ;rtp=rtp_new
      ;bmod=ff
      
        n=n_elements(bmod)
        ; Conversion rtp->xyz et normalisation
        rtp(*,1:2)=rtp(*,1:2)*!dtor
        bz=rebin([1.,0.,0.],3,n)
      
        b(0,*)/=bmod
        b(1,*)/=bmod
        b(2,*)/=bmod
        convert,b,b2,rtp,n
        b=b2
        convert,bz,b2,rtp,n
        bz=b2
        convert,gb,b2,rtp,n
        gb=b2
        
        bz=bz-total(bz*b)*b
        bz=bz/sqrt(total(bz^2))
      
        gbz=total(gb*bz,1)
        gbb=total(gb*b,1)
        gb=atan(gbz,gbb)
      print,"dist ",p," long ",lg," North n",n,' fmax ',max(bmod)
      x=float(x)
      y=float(y)
      z=float(z)
      b=float(b)
      bz=float(bz)
      bmod=float(bmod)
      gb=float(gb)
      sc_gb=float(sc_gb)  
      
      if keyword_set(moon) then if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/'+moon+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/'+moon+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr' else $
        if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
      
        openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
        writeu,unit,n
        writeu,unit,x
        writeu,unit,y
        writeu,unit,z
        writeu,unit,b
        writeu,unit,bmod
        writeu,unit,sc_gb
        writeu,unit,bz
        writeu,unit,gb
        close, unit & free_lun, unit
        
        L_SHELL_TEST_N,-p,lg,90.,rtp,x,y,z,B,bmod,gb,mag_step,mfl_model=mfl_model,lsh=0,dip_param=dipole_param
      
      ;srt=sort(bmod)
      ;bmod=bmod(srt)
      ;b=b[srt,*]
      ;gb=gb[srt,*]
      ;x=x[srt]
      ;y=y[srt]
      ;z=z[srt]
      ;rtp=rtp[srt,*]
      
        bmod=bmod*fsb
        b=b*fsb
        b=transpose(b)
        gb=transpose(gb)
        sc_gb=sqrt(total(gb^2,1))
        gb[0,*]/=sc_gb
        gb[1,*]/=sc_gb
        gb[2,*]/=sc_gb
        sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)
      
      ; LL 21/07/15 : rŽinterpolation pose problme quand la variation de bmod n'est pas dŽcroissante, idem plus haut
      ;               rŽ-Žchantillonage supprimŽ pour le moment
      ;nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
      ;mff=max(bmod)-nff*mag_step
      ;ff=findgen(nff+1)*mag_step+mff-mag_step/2. 
      ;b_new=dblarr(3,nff+1)
      ;rtp_new=dblarr(nff+1,3)
      ;gb_new=fltarr(3,nff+1)
      ;x= interpol(x,bmod,ff)
      ;y= interpol(y,bmod,ff)
      ;z= interpol(z,bmod,ff)
      ;sc_gb= interpol(sc_gb,bmod,ff)
      ;gb_new[0,*] = interpol(gb[0,*],bmod,ff)
      ;gb_new[1,*] = interpol(gb[1,*],bmod,ff)
      ;gb_new[2,*] = interpol(gb[2,*],bmod,ff)
      ;gb=gb_new
      ;b_new[0,*] = interpol(b[0,*],bmod,ff)
      ;b_new[1,*] = interpol(b[1,*],bmod,ff)
      ;b_new[2,*] = interpol(b[2,*],bmod,ff)
      ;b=b_new
      ;rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
      ;rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
      ;rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
      ;rtp=rtp_new
      ; bmod=ff
      
        n=n_elements(bmod)
      
        ; Conversion rtp->xyz et normalisation
        rtp(*,1:2)=rtp(*,1:2)*!dtor
        bz=rebin([1.,0.,0.],3,n)
      
        b(0,*)/=bmod
        b(1,*)/=bmod
        b(2,*)/=bmod
        convert,b,b2,rtp,n
        b=b2
        convert,bz,b2,rtp,n
        bz=b2
        convert,gb,b2,rtp,n
        gb=b2
        
        bz=bz-total(bz*b)*b
        bz=bz/sqrt(total(bz^2))
      
        gbz=total(gb*bz,1)
        gbb=total(gb*b,1)
        gb=atan(gbz,gbb)
      
      print,"dist ",p," long ",lg," South n",n,' fmax ',max(bmod)
      
      x=float(x)
      y=float(y)
      z=float(z)
      b=float(b)
      bz=float(bz)
      bmod=float(bmod)
      gb=float(gb)
      sc_gb=float(sc_gb)
      
      if keyword_set(moon) then if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/-'+moon+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/'+moon+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr' else $
        if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/-'+STRSPLIT(p_directory,/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_lsh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
        openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
        writeu,unit,n
        writeu,unit,x
        writeu,unit,y
        writeu,unit,z
        writeu,unit,b
        writeu,unit,bmod
        writeu,unit,sc_gb
        writeu,unit,bz
        writeu,unit,gb
        close, unit & free_lun, unit
      endfor
  endfor
endif

if keyword_set(msh) then begin
  ;(3) msh
  
  ;#########
  
  
  for p=l1,l2,l_step do begin

    if (axi[c_model]) then lgmax=0 else begin
      if (fix(p)-p eq 0) then p_directory = fix(p) $
      else  p_directory = string(format = '(F4.2)',p) 
      if keyword_set(moon) then cmd='mkdir '+dossier+models[c_model]+'_msh'+filenametest+'/'+moon else $
        cmd='mkdir '+dossier+models[c_model]+'_msh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)


      spawn,cmd,resu
      
      print,"trying ",cmd," got",resu
    endelse
    for lg=lIIIWest1,lIIIWest2 do begin
        theta=(dip[0])*cos((dip[1]-lg)*!dtor)+90.
      
        L_SHELL_TEST_N,p,lg,theta,rtp,x,y,z,B,bmod,gb,mag_step,mfl_model=mfl_model,msh=1,dip_param=dipole_param
      ; LL 21/07/15 : rŽordonnement pose problme quand la variation de bmod n'est pas dŽcroissante
      ;               Ex : Io nord, longi = 50¡
      ;srt=sort(bmod)
      ;bmod=bmod(srt)
      ;b=b[srt,*]
      ;gb=gb[srt,*]
      ;x=x[srt]
      ;y=y[srt]
      ;z=z[srt]
      ;rtp=rtp[srt,*]
      
        bmod=bmod*fsb
        b=b*fsb
        b=transpose(b)
        gb=transpose(gb)
      
        sc_gb=sqrt(total(gb^2,1))
        gb[0,*]/=sc_gb
        gb[1,*]/=sc_gb
        gb[2,*]/=sc_gb
        sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)
      
      ; LL 21/07/15 : rŽinterpolation pose problme quand la variation de bmod n'est pas dŽcroissante, idem plus haut
      ;               rŽ-Žchantillonage supprimŽ pour le moment
      ;nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
      ;mff=max(bmod)-nff*mag_step
      ;ff=findgen(nff+1)*mag_step+mff
      ;b_new=dblarr(3,nff+1)
      ;rtp_new=dblarr(nff+1,3)
      ;gb_new=fltarr(3,nff+1)
      ;x= interpol(x,bmod,ff)
      ;y= interpol(y,bmod,ff)
      ;z= interpol(z,bmod,ff)
      ;sc_gb= interpol(sc_gb,bmod,ff)
      ;gb_new[0,*] = interpol(gb[0,*],bmod,ff)
      ;gb_new[1,*] = interpol(gb[1,*],bmod,ff)
      ;gb_new[2,*] = interpol(gb[2,*],bmod,ff)
      ;gb=gb_new
      ;b_new[0,*] = interpol(b[0,*],bmod,ff)
      ;b_new[1,*] = interpol(b[1,*],bmod,ff)
      ;b_new[2,*] = interpol(b[2,*],bmod,ff)
      ;b=b_new
      ;rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
      ;rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
      ;rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
      ;rtp=rtp_new
      ;bmod=ff
      
        n=n_elements(bmod)
        ; Conversion rtp->xyz et normalisation
        rtp(*,1:2)=rtp(*,1:2)*!dtor
        bz=rebin([1.,0.,0.],3,n)
      
        b(0,*)/=bmod
        b(1,*)/=bmod
        b(2,*)/=bmod
        convert,b,b2,rtp,n
        b=b2
        convert,bz,b2,rtp,n
        bz=b2
        convert,gb,b2,rtp,n
        gb=b2
        
        bz=bz-total(bz*b)*b
        bz=bz/sqrt(total(bz^2))
      
        gbz=total(gb*bz,1)
        gbb=total(gb*b,1)
        gb=atan(gbz,gbb)
      print,"dist ",p," long ",lg," North n",n,' fmax ',max(bmod)
      x=float(x)
      y=float(y)
      z=float(z)
      b=float(b)
      bz=float(bz)
      bmod=float(bmod)
      gb=float(gb)
      sc_gb=float(sc_gb)  
      
      if keyword_set(moon) then if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/'+moon+'.lsr' else nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/'+moon+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr' else $
        if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)+'/'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
      
        openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
        writeu,unit,n
        writeu,unit,x
        writeu,unit,y
        writeu,unit,z
        writeu,unit,b
        writeu,unit,bmod
        writeu,unit,sc_gb
        writeu,unit,bz
        writeu,unit,gb
        close, unit & free_lun, unit
        
        L_SHELL_TEST_N,-p,lg,theta,rtp,x,y,z,B,bmod,gb,mag_step/fsb,mfl_model=mfl_model,msh=1,dip_param=dipole_param
      
      ;srt=sort(bmod)
      ;bmod=bmod(srt)
      ;b=b[srt,*]
      ;gb=gb[srt,*]
      ;x=x[srt]
      ;y=y[srt]
      ;z=z[srt]
      ;rtp=rtp[srt,*]
      
        bmod=bmod*fsb
        b=b*fsb
        b=transpose(b)
        gb=transpose(gb)
      
        sc_gb=sqrt(total(gb^2,1))
        gb[0,*]/=sc_gb
        gb[1,*]/=sc_gb
        gb[2,*]/=sc_gb
        sc_gb=sc_gb*fsb/(bmod>1E-10);*(c/rp/((bmod*1E6)>1E-10)) ;(grab B /B  *c/fc)
      
      ; LL 21/07/15 : rŽinterpolation pose problme quand la variation de bmod n'est pas dŽcroissante, idem plus haut
      ;               rŽ-Žchantillonage supprimŽ pour le moment
      ;nff=fix((max(bmod)-max([min(bmod),mag_step]))/magstep)
      ;mff=max(bmod)-nff*mag_step
      ;ff=findgen(nff+1)*mag_step+mff-mag_step/2. 
      ;b_new=dblarr(3,nff+1)
      ;rtp_new=dblarr(nff+1,3)
      ;gb_new=fltarr(3,nff+1)
      ;x= interpol(x,bmod,ff)
      ;y= interpol(y,bmod,ff)
      ;z= interpol(z,bmod,ff)
      ;sc_gb= interpol(sc_gb,bmod,ff)
      ;gb_new[0,*] = interpol(gb[0,*],bmod,ff)
      ;gb_new[1,*] = interpol(gb[1,*],bmod,ff)
      ;gb_new[2,*] = interpol(gb[2,*],bmod,ff)
      ;gb=gb_new
      ;b_new[0,*] = interpol(b[0,*],bmod,ff)
      ;b_new[1,*] = interpol(b[1,*],bmod,ff)
      ;b_new[2,*] = interpol(b[2,*],bmod,ff)
      ;b=b_new
      ;rtp_new[*,0] = interpol(rtp[*,0],bmod,ff)
      ;rtp_new[*,1] = interpol(rtp[*,1],bmod,ff)
      ;rtp_new[*,2] = interpol(rtp[*,2],bmod,ff)
      ;rtp=rtp_new
      ; bmod=ff
      
        n=n_elements(bmod)
      
        ; Conversion rtp->xyz et normalisation
        rtp(*,1:2)=rtp(*,1:2)*!dtor
        bz=rebin([1.,0.,0.],3,n)
      
        b(0,*)/=bmod
        b(1,*)/=bmod
        b(2,*)/=bmod
        convert,b,b2,rtp,n
        b=b2
        convert,bz,b2,rtp,n
        bz=b2
        convert,gb,b2,rtp,n
        gb=b2
        
        bz=bz-total(bz*b)*b
        bz=bz/sqrt(total(bz^2))
      
        gbz=total(gb*bz,1)
        gbb=total(gb*b,1)
        gb=atan(gbz,gbb)
      
      print,"dist ",p," long ",lg," South n",n,' fmax ',max(bmod)
      
      x=float(x)
      y=float(y)
      z=float(z)
      b=float(b)
      bz=float(bz)
      bmod=float(bmod)
      gb=float(gb)
      sc_gb=float(sc_gb)
      
      if keyword_set(moon) then if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/-'+moon+'.lsr' else nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/'+moon+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr' else $
        if axi[c_model] then   nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/-'+STRSPLIT(p_directory,/EXTRACT)+'.lsr' else nom_fichier=dossier+models[c_model]+'_msh'+filenametest+'/'+STRSPLIT(p_directory,/EXTRACT)+'/-'+STRSPLIT(STRING(fix(lg)),/EXTRACT)+'.lsr'
        openw, unit, nom_fichier, /get_lun,/swap_if_little_endian
        writeu,unit,n
        writeu,unit,x
        writeu,unit,y
        writeu,unit,z
        writeu,unit,b
        writeu,unit,bmod
        writeu,unit,sc_gb
        writeu,unit,bz
        writeu,unit,gb
        close, unit & free_lun, unit
    endfor
  endfor
  
endif


endfor

return
end
