pro COEFF_JRM09_CSCon2020,g,h,n,dip,crt,tdip

;# coefficient model JRM09, https://doi.org/10.1002/2018GL077312
;# g et h: coeff. of Legendre Polynomials
;# n: development order
;# dip:position of the du dipole wrt the rotation axis
;# crt: current sheet model based on Connerney 2020, https://doi.org/10.1029/2020JA028138

n=10

; # calling the jrm09 routine to obtain the g,h,dip variables
;# Only The current sheet (crt) will be changed here (crt)
coeff_jrm09,g,h,n,dip,crt,tdip


;# current sheet parameters:
;# crt[0]: Disc inner radius
;# crt[1]: Disc outer radius
;# crt[2]: Disc half thickness
;# crt[3]: Disc current constant (mu0I/2)
;# crt[4]: radial current system contributing to the azimuthal magnetic field
crt=[7.8,51.4,3.6,139.6,16.7]
return
end
