# First release (1.0)

New features:
- Merge magnetic field lines computation routines (CALC_LIGNE_SAT & CALC_LIGNE_JUP) into CALC_MFL.pro
- Option to select Jovian magnetic field lines  (M-shell, L-Shell, Moons)
- Option to include a current sheet model
- Mercury magnetic field model (A12)
- Earth magnetic field models (IGRF1995 & IGRF2000)
- Option in CALC_MFL.pro to have a direct input of the dipole parameters
- JRM09 + Connerney et al. 2020 Current sheet model 
- Current sheet model: model of radial current component with adjustement of Bphi
- Add License

Fix from previous internal version:
- Translate some comments into english
- Fixed call to isaac magnetic field coefficients
- Fixing JRM09 magnetic field rotation matrix
- Remove all *_old.pro files
- Remove all *.sav files
